# RAPID - RESTFul API Documentation Builder

### Installation
Run the following command from the project folder to install all required dependencies:

    npm install --save

### Setup
__RAPID__ uses __Postgres SQL__ as a database engine, therefore in order to set it up,
one should have __Postgres__ up and running. All required connection settings
can be found and changed in __config.js__.

Once __Postgres__ is configured, initial project schema and an admin user should
be created by running __setup__:

    npm run-script setup

### How to start
    npm start

### Documentation
Full manual is available [here](http://rapid-api.me/documentation).

### Tests
    npm test

### Deployment
Before deploying, make sure you have run the __browserify__ script:

    npm run-script browserify

---

### License
This product is licensed under the MIT license
