var request = require('request');
var db = require('../db/db');
var encryption = require('../db/encryption');
var accessControl = require('../middleware/access_control');
var emailSender = require('../middleware/email.js');
var express = require('express');
var config = require('./../config.js');
var router = express.Router();


// A User controller for managing users
function UserController() {
    this.index = function(req, res) {
        if (err) {
            return res.renderError(500, req, res);
        }
        
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            client.query('SELECT * FROM api_user', function(err, result) {
                // Done working with the db client
                done();

                if (err) {
                    return res.renderError(500, req, res);
                }

                res.render('user/index.html', {
                    users: result.rows,
                    user: req.user
                });
            });
        });
    };

    this.new = function(req, res) {
        if (req.user.username != 'Guest') {
            return res.redirect('/project');
        }

        res.render('user/user.html', {
            user: req.user
        });
    };

    this.create = function(req, res) {
        var salt = encryption.salt();
        var token = encryption.salt();

        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            request.post('https://www.google.com/recaptcha/api/siteverify', {
                    form: {
                        secret: config.captcha_secret,
                        response: req.body['g-recaptcha-response']
                    }
                },
                function(error, response, body) {
                    // Captcha has been confirmed, can create a user now
                    if (!error && response.statusCode == 200 && JSON.parse(body).success === true) {
                        createUser();
                    } else {
                        errorResponse('Failed to verify captcha. Please try again.');
                    }
                }
            );

            // Error while creating user 
            function errorResponse(message) {
                return res.render('user/user.html', {
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    user: req.user,
                    message: message,
                    username: req.body.username,
                    password: req.body.password
                });
            }
            
            function createUser () {
                client.query(
                        'INSERT INTO api_user (username, first_name, last_name, password_digest, salt, token) ' +
                        'VALUES (($1),($2),($3),($4),($5), ($6)) RETURNING id;', [
                        req.body.username,
                        req.body.first_name,
                        req.body.last_name,
                        encryption.digest(req.body.password + salt),
                        salt,
                        token
                        ], function(err, result) {
                            // Done working with the db client
                            done();

                            if (err) {
                                return res.render('user/user.html', {
                                    user: req.user,
                                    message: 'Error occured creating a new user',
                                    username: req.body.username,

                                    // if username has been taken already
                                    taken: (err.toString().indexOf('duplicate') >= 0 ? 'This username is taken' : undefined),

                                    first_name: req.body.first_name,
                                    last_name: req.body.last_name
                                });
                            }
                            // Start by verifying captcha 
                            emailSender.sendActivationEmail(
                                    req.body.username,
                                    req.body.first_name,
                                    req.body.last_name,
                                    token
                                    );

                            req.session.reset();
                            res.render('email/activate_message.html', {
                                user: {
                                    username: 'Guest'
                                },
                                email: req.body.username
                            });
                        }
                );
            }
        });
    };

    this.verify = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            console.log('req.query: ', req.query);
            client.query(
                'SELECT * FROM api_user WHERE token = ($1)', 
                [req.query.token], 
                function(err, result) {
                    if (err) {
                        done();
                        return res.renderError(500, req, res);
                    }

                    var user = result.rows[0];
                    if (!user) {
                        done();
                        return res.renderError(403, req, res);
                    }

                    client.query(
                        'UPDATE api_user SET is_activated = true WHERE token = ($1)', 
                        [req.query.token], 
                        function(err, result) {
                            done();

                            if (err) {
                                return res.renderError(500, req, res);
                            }

                            req.session.user_id = user.id;
                            res.redirect('/project');
                        }
                    );
                }
            );
        });
    };

    this.edit = function(req, res) {
        req.user['edit_mode'] = true;

        if (req.query.mode == 'json') {
            res.json(req.user);
        }
        else {
            res.render('user/user.html', req.user);
        }
    };

    this.update = function(req, res) {
        // update user profile
        // Need a user if I want to update the password
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query('select * from api_user WHERE id = ($1)', [req.params.user_id], function(err, result) {
                if (err) {
                    done();
                    return res.renderError(500, req, res);
                }

                var user = result.rows[0];

                if (!user) {
                    done();
                    return res.renderError(404, req, res);
                }

                if (user.id != req.session.user_id) {
                    done();
                    return res.renderError(403, req, res);
                }

                var password = user.password_digest;
                var updatePassword = req.body.old_password && req.body.password;
                // User wants to change a password
                if (updatePassword) {
                    // His old_password value is correct
                    if (encryption.digest(req.body.old_password + user.salt) == user.password_digest) {
                        password = encryption.digest(req.body.password + user.salt);
                    }
                    else {
                        return res.render('user/user.html', {
                        edit_mode: true,
                        user: req.user,
                        message: 'Old password value does not match our records',
                        username: req.body.username,
                        first_name: req.body.first_name,
                        last_name: req.body.last_name
                        });
                    }
                }

                // Got a user, now let's update his/her stuff
                client.query(
                    'UPDATE api_user SET first_name = ($1), last_name = ($2), ' +
                    'password_digest = ($3) WHERE id = ($4)', [
                    req.body.first_name,
                    req.body.last_name,
                    password,
                    user.id
                ], function(err, result) {
                    // Done working with the db client
                    done();

                    if (err) {
                        return res.renderError(500, req, res);
                    }

                    return res.render('user/user.html', {
                        edit_mode: true,
                        success: true,
                        user: req.user,
                        message: (updatePassword ?
                            'Password has been changed' : 'Profile has been successfully updated'),
                        username: req.body.username,
                        first_name: req.body.first_name,
                        last_name: req.body.last_name
                    });
                });
            });
        });
    };

    this.destroy = function(req, res) {
        if (!req.user.admin) {
            return res.renderError(403, req, res);
        }

        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query('DELETE FROM user CASCADE WHERE id=($1)', [req.params.id], function(err, result) {
                // Done working with the db client
                done();

                if (err) {
                    return res.renderError(500, req, res);
                }

                res.redirect('/user');
            });
        });
    };

    this.redirect = function(req, res) {
        res.status(301).send('This page has moved to <a href="/project">Projects</a>');
    };

    this.search = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            // search for activated users using email, first name and last name
            client.query(
                "SELECT id, username, first_name, last_name " +
                "FROM api_user " +
                "WHERE (username ILIKE '%' || ($1) || '%' " +
                "OR first_name ILIKE '%' || ($1) || '%' " +
                "OR last_name ILIKE '%' || ($1) || '%') " +
                "AND is_activated = true LIMIT 10;",
                [req.body.search_query],
                function(err, result) {
                    done();

                    if (err) {
                        return res.renderError(500, req, res);
                    }

                    // return the results of the search
                    res.json(result.rows);
                }
            );
            
        });
    };
    
}


// Routes
var userController = new UserController();
router.get('/', accessControl.adminOnly, userController.index);
router.get('/new', userController.new);
router.post('/', userController.create);
router.post('/search', accessControl.noGuests, userController.search);
router.get('/:user_id/edit', accessControl.userCorrespondsWithSession, userController.edit);
router.post('/:user_id', userController.update);
router.get('/:user_id/delete', accessControl.userCorrespondsWithSession, userController.destroy);
router.get('/verify', userController.verify);

module.exports = exports = router;
