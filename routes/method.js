var db = require('../db/db');
var accessControl = require('../middleware/access_control');
var marked = require("marked");
var express = require('express');
var router = express.Router();


// The method controller is responsible for handling all method-related operations
function MethodController() {
    var loadSideMenuItems = function(req, res, client, done, projectId, callback) {
        // Query pages and modules
        client.query(
            'SELECT m.*, p.owner_id from module AS m JOIN project AS p ON p.id = project_id ' +
            'WHERE project_id = ($1) ORDER BY rank ASC, title ASC', [projectId],
            function(err, result) {
                if (err) {
                    done();

                    return res.renderError(500, req, res);
                }

                // save modules
                var modules = result.rows.filter(function(module) {
                    return module.owner_id == req.session.user_id || !module.is_private;
                });

                client.query(
                    'SELECT pg.*, p.owner_id from page AS pg JOIN project AS p ON p.id = project_id ' +
                    'WHERE project_id = ($1) ORDER BY rank ASC, title ASC', [projectId],
                    function(err, result) {
                        // Done working with the db client
                        done();
                        if (err) {
                            return res.renderError(500, req, res);
                        }

                        // save pages
                        var pages = result.rows.filter(function(page) {
                            return page.owner_id == req.session.user_id || !page.is_private;
                        });

                        // Respond to the callback
                        callback(modules, pages);
                    }
                );
            }
        );
    };

    this.show = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query(
                'SELECT m.*, p.owner_id, p.title AS project_title, mo.title AS module_title, ' +
                'mo.is_private AS module_is_private, p.is_private AS project_is_private FROM method AS m ' +
                'JOIN project AS p ON p.id = m.project_id ' +
                'JOIN module AS mo ON mo.id = m.module_id ' +
                'WHERE m.id = ($1)', [req.params.method_id],
                function(err, result) {
                    if (err) {
                        done();
                        return res.renderError(500, err);
                    }

                    var queryResult = result.rows[0];

                    if (!queryResult) {
                        done();
                        return res.renderError(404, req, res);
                    }

                    if ((queryResult.project_is_private || queryResult.module_is_private || queryResult.is_private)
                        && queryResult.owner_id != req.user.id) {
                        done();
                        return res.renderError(403, req, res);
                    }

                    client.query(
                        'SELECT * FROM project WHERE id = ($1)', [queryResult.project_id], function(err, result) {
                        if (err) {
                            done();
                            return res.renderError(500, req, res);
                        }

                        if (!result) {
                            done();
                            return res.renderError(404, req, res);
                        }
                        
                        var project = result.rows[0];

                        // Load modules and pages
                        loadSideMenuItems(req, res, client, done, queryResult.project_id, function(modules, pages) {
                            // convert JSON parameters and messages into JS objects/arrays
                            queryResult.parameters = JSON.parse(queryResult.parameters);
                            queryResult.response_messages = JSON.parse(queryResult.response_messages);
                            project.response_messages = JSON.parse(project.response_messages);

                            // Format full description (if any)
                            if (queryResult.full_description) {
                                queryResult.full_description = marked(queryResult.full_description);
                            }

                            var projectResponsesForMethod = [];
                            project.response_messages.forEach(function(index) {
                                if (queryResult.project_codes.indexOf(index.code) != -1) {
                                    projectResponsesForMethod.push(index);
                                }
                                
                            });
                            project['response_messages'] = projectResponsesForMethod;
                            
                            // everything is fine
                            var returnDict = {
                                title: '/' + queryResult.url_name,
                                user: req.user,
                                owner_id: queryResult.owner_id,
                                project_id: queryResult.project_id,
                                project: project, 
                                modules: modules,
                                pages: pages,
                                method: queryResult,
                                show_mode: true
                            }

                            if (req.query.mode == 'json') {
                                res.json(returnDict);
                            }
                            else {
                                res.render('method/show.html', returnDict);
                            }
                        });
                    });
                }
            );
        });
    };

    this.new = function(req, res, next) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // Everything is fine, now we can actually return the page content to show
            client.query(
                'SELECT m.*, p.owner_id FROM module AS m ' +
                'JOIN project AS p on p.id = m.project_id WHERE m.id = ($1)', [req.params.module_id],
                function(err, result) {
                    if (err) {
                        done();
                        res.renderError(500, req, res);
                    }
                    
                    var queryResult = result.rows[0];

                    if (!queryResult) {
                        done();
                        return res.renderError(404, req, res);
                    }

                    if (queryResult.owner_id != req.user.id) {
                        done();
                        return res.renderError(403, req, res);
                    }

                    //get the project
                    client.query(
                        'SELECT * FROM project WHERE id = ($1)', [queryResult.project_id], function(err, result) {
                            done();

                            if (err) {
                                return res.renderError(500, req, res);
                            }
                            
                            var project = result.rows[0];
                            if (!project) {
                                return res.renderError(404, req, res);
                            }
                            
                            // parse the response codes
                            project.response_messages = JSON.parse(project.response_messages);
                            // Everything is fine, now we can actually show the new method form
                            res.render('method/method.html', {
                                title: 'New method',
                                user: req.user,
                                module_id: req.params.module_id,
                                project: project
                        });
                    });
                }
            );
        });
    };

    this.edit = function(req, res) {
        // test
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // Everything is fine, now we can actually return the page content to show
            client.query('SELECT * FROM method WHERE id = ($1)', [req.params.method_id], function(err, result) {
                if (err) {
                    done();
                    return res.renderError(500, req, res);
                }

                // Get the method
                var method = result.rows[0];
                if (!method) {
                    done();
                    return res.renderError(404, req, res);
                }

                // Get the project and verify that the user can edit the method
                client.query('SELECT * FROM project WHERE id = ($1)', [method.project_id], function(err, result) {
                    done();
                    var project = result.rows[0];

                    if (err || !project) {
                        return res.renderError(500, req, res);
                    }

                    if (project.owner_id != req.user.id) {
                        return res.renderError(403, req, res);
                    }

                    // convert JSON parameters and messages into JS objects/arrays
                    method.parameters = JSON.parse(method.parameters);
                    method.response_messages = JSON.parse(method.response_messages);
                    project.response_messages = JSON.parse(project.response_messages);

                    project.response_messages.forEach(function(index) {
                        if (method.project_codes.indexOf(index.code) != -1) {
                            index['isChecked'] = true;
                        }
                        else{
                            index['isChecked'] = false;
                        }
                    });

                    res.render('method/method.html', {
                        title: '/' + method.url_name,
                        edit_mode: true,
                        user: req.user,
                        method: method,
                        project: project
                    });
                });
            });
        });
    };

    this.create = function(req, res) {
        // parse the json data with method info
        var method = JSON.parse(req.body.method);

        // Perform the database actions
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // Get the project and verify that the user can create a method
            client.query(
                'SELECT m.*, p.owner_id FROM module AS m ' +
                'JOIN project AS p on p.id = m.project_id WHERE m.id = ($1)', [req.params.module_id],
                function(err, result) {
                    var queryResult = result.rows[0];

                    if (err) {
                        done();
                        return res.status(500).end('Please check the entered data');
                    }

                    if (!queryResult) {
                        done();
                        return res.status(404).end('Module for this method is not found');
                    }

                    if (queryResult.owner_id != req.user.id) {
                        done();
                        return res.status(403).end('You have no rights to do this');
                    }

                    var rank = method.rank == undefined || method.rank > 100 ? 100 : method.rank;
                    // Everything is fine, now we can actually store the method
                    client.query(
                        'INSERT INTO method (url_name, rank, request_type, short_description, full_description, ' +
                        'response, response_type, parameters, is_private, project_id, module_id, response_messages, project_codes) ' +
                        'VALUES (($1),($2),($3),($4),($5),($6),($7),($8),($9),($10),($11),($12),($13)) RETURNING id;', [
                            method.url_name, rank, method.request_type, method.short_description, method.full_description,
                            method.response, method.response_type, method.parameters, 
                            method.is_private, queryResult.project_id, queryResult.id, method.response_messages, method.project_codes
                        ],
                        function(err, result) {
                            // Done working with the db client
                            done();

                            if (err) {
                                return res.renderError(500, err);
                            }

                            // Return the url to redirect - SHOW method page
                            res.json({url: '/method/' + result.rows[0].id});
                        }
                    );
                }
            );
        });
    };

    this.update = function(req, res) {
        // parse the json data with method info
        var method = JSON.parse(req.body.method);
        // Perform the database actions
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // Get the project and verify that the user can update a method
            client.query(
                'SELECT m.id, p.owner_id FROM method AS m ' +
                'JOIN project AS p on p.id = m.project_id WHERE m.id = ($1)', [req.params.method_id],
                function(err, result) {
                    var queryResult = result.rows[0];

                    if (err) {
                        done();
                        return res.status(500).end('Please check the entered data');
                    }
                    
                    if (!queryResult) {
                        done();
                        return res.status(404).end('Not found');
                    }
                    
                    if (queryResult.owner_id != req.user.id) {
                        done();
                        return res.status(403).end('You have no rights to do this');
                    }
                    
                    // Everything is fine, now we can actually store the method
                    client.query(
                        'UPDATE method SET ' +
                        'url_name = ($1), request_type = ($2), short_description = ($3), ' +
                        'full_description = ($4), response = ($5), response_type = ($6), rank = ($7), ' + 
                        'parameters = ($8), is_private = ($9), response_messages = ($10), project_codes = ($11) WHERE id = ($12)', [
                            method.url_name, method.request_type, method.short_description, method.full_description,
                            method.response, method.response_type, method.rank, method.parameters, 
                            method.is_private, method.response_messages, method.project_codes, queryResult.id
                        ],
                        function(err, result) {
                            // Done working with the db client
                            done();

                            if (err) {
                                return res.status(500).end('Please check the entered data');
                            }
                            
                            // Return the url to redirect - SHOW method page
                            res.json({url: '/method/' + queryResult.id});
                        }
                    );
                }
            );
        });
    };

    this.destroy = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query(
                'SELECT m.id, m.project_id, m.module_id, p.owner_id FROM method AS m ' +
                'JOIN project AS p on p.id = m.project_id WHERE m.id = ($1)',
                [req.params.method_id],
                function(err, result) {
                    if (err) {
                        done();
                        
                        return res.renderError(500, req, res);
                    }

                    // Get the queryResult
                    var queryResult = result.rows[0];
                    if (!queryResult) {
                        done();
                        
                        return res.redirect('/project');
                    }
                    else if (queryResult.owner_id != req.user.id) {
                        done();
                        
                        return res.renderError(403, req, res);
                    }

                    // Got the method, let's delete it
                    client.query('DELETE FROM method WHERE id = ($1)', [req.params.method_id], function(err, result) {
                        // Done working with the db client
                        done();

                        if (err) {
                            return res.renderError(500, req, res);
                        }

                        res.redirect('/module/' + queryResult.module_id);
                    });
                }
            );
        });
    };
}


// Routes
var methodController = new MethodController();
router.get('/:method_id', methodController.show);
router.post('/:method_id', accessControl.noGuests, methodController.update);
router.get('/:method_id/edit', accessControl.noGuests, methodController.edit);
router.get('/:method_id/delete', accessControl.noGuests, methodController.destroy);
router.post('/module/:module_id', accessControl.noGuests, methodController.create);
router.get('/module/:module_id/new', accessControl.noGuests, methodController.new);


module.exports = exports = router;
