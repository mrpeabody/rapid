var db = require('../db/db');
var accessControl = require('../middleware/access_control');
var marked = require('marked');
var striptags = require('striptags');
var permissions = require('../middleware/permissions');
var express = require('express');
var router = express.Router();


// A project controller for handling projects (create/list/delete/etc)
function ProjectController() {
    var loadSideMenuItems = function(req, res, client, done, projectId, callback) {
        // Query pages and modules
        client.query(
            'SELECT m.*, p.owner_id from module AS m JOIN project AS p ON p.id = project_id ' +
            'WHERE project_id = ($1) ORDER BY rank ASC, title ASC', [projectId],
            function(err, result) {
                if (err) {
                    done();
                    
                    return res.renderError(500, req, res);
                }

                // save modules
                var modules = result.rows.filter(function(module) {
                    return module.owner_id == req.session.user_id || !module.is_private;
                });

                client.query(
                    'SELECT pg.*, p.owner_id from page AS pg JOIN project AS p ON p.id = project_id ' +
                    'WHERE project_id = ($1) ORDER BY rank ASC, title ASC', [projectId],
                    function(err, result) {
                        // Done working with the db client
                        done();

                        if (err) {
                            return res.renderError(500, req, res);
                        }

                        // save pages
                        var pages = result.rows.filter(function(page) {
                            return page.owner_id == req.session.user_id || !page.is_private;
                        });

                        // Respond to the callback
                        callback(modules, pages);
                    }
                );
            }
        );
    };

    this.index = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query(
                'SELECT p.id, p.title, p.description, p.owner_id, au.username, p.date_created, p.date_modified ' +
                'FROM project AS p JOIN api_user AS au ON au.id = owner_id WHERE owner_id = ($1)', [req.user.id],
                function(err, result) {
                    // Done working with the db client
                    done();

                    if (err) {
                        return res.renderError(500, req, res);
                    }

                    var returnDict = {
                        user: req.user,
                        projects: result.rows.map(function(project) {
                            project.description = striptags(marked(project.description));
                            return project;
                        })
                    }

                    if (req.query.mode == 'json') {
                        res.json(returnDict);
                    }
                    else {
                        res.render('project/index.html', returnDict);
                    }
                }
            );
        });
    };

    this.show = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query('SELECT * FROM project WHERE id = ($1)', [req.params.project_id], function(err, result) {
                if (err) {
                    done();
                    return res.renderError(500, req, res);
                }

                // Get the project
                var project = result.rows[0];
                if (!project) {
                    done();
                    return res.renderError(404, req, res);
                }
                else if (project.is_private && project.owner_id != req.user.id) {
                    done();
                    return res.renderError(403, req, res);
                }

                // Load modules and pages
                loadSideMenuItems(req, res, client, done, project.id, function(modules, pages) {
                    // Convert description into formatted text
                    project.description = marked(project.description);

                    project.response_messages = JSON.parse(project.response_messages);

                    var outputDict = {
                        title: project.title,
                        user: req.user,
                        project: project,
                        project_id: project.id,
                        owner_id: project.owner_id,
                        modules: modules,
                        pages: pages
                    };

                    if (req.query.mode == 'json') {
                        res.json(outputDict);
                    }
                    else {
                        res.render('project/show.html', outputDict);
                    }
                });
            });
        });
    };

    this.new_form = function(req, res) {
        res.render('project/project.html', {user: req.user});
    };

    this.create = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // parse the body json to get the project information
            var project = JSON.parse(req.body.project);

            client.query(
                'INSERT INTO project (title, description, url, is_private, owner_id, response_messages) ' +
                'VALUES (($1),($2),($3),($4),($5),($6)) RETURNING id;', [
                    project.title && project.title.length ? project.title : null,
                    project.description,
                    project.url,
                    project.is_private ? true : false,
                    req.user.id,
                    project.response_messages
                ],
                function(err, result) {

                    if (err) {
                        done();
                        return res.render('project/project.html', {
                            user: req.user,
                            project: project,
                            message: 'Please check the entered information again, then submit'
                        });
                    }

                    // no error now add permissions
                    var project_id = result.rows[0].id;
                    client.query(
                        'INSERT INTO permission (project_id, user_id, level) values (($1), ($2), ($3))', [project_id, req.user.id, 3],
                        function(err, result) {
                            done();
                            
                            if (err) {
                                return res.render('project/project.html', {
                                    user: req.user,
                                    project: req.body,
                                    message: "Error creating permissions"
                                });
                            }
                            
                            // res.redirect('/project/' + project_id);
                            res.json({
                                url: project_id
                            });
                        }
                    );
                }
            );
        });
    };

    this.update = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // parse the body json to get the project information
            var project = JSON.parse(req.body.project);

            client.query(
                'UPDATE project SET title = ($1), description = ($2), url = ($3), is_private = ($4), response_messages = ($5) WHERE id = ($6)', [
                    project.title && project.title.length ? project.title : null,
                    project.description,
                    project.url,
                    project.is_private ? true : false,
                    project.response_messages,
                    req.params.project_id
                ], function(err, result) {
                    // Done working with the db client
                    done();

                    if (err) {
                        return res.render('project/project.html', {
                            user: req.user,
                            project: project,
                            edit_mode: true,
                            message: 'Please check the entered information again, then submit'
                        });
                    }

                    res.json({
                        url: '/project/' + req.params.project_id
                    });
                }
            );
        });
    };

    this.edit = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            permissions.projectWithPermissions(
                req, res, client, done, req.user, req.params.project_id, 
                function(project) {
                    if (err) {
                        done();
                        return res.renderError(500, req, res);
                    }

                    client.query(
                        'SELECT api_user.id, username, first_name, last_name, level ' +
                            'FROM permission ' +
                            'JOIN api_user ON api_user.id = user_id ' +
                            'WHERE project_id = ($1);',
                        [req.params.project_id], 
                        function(err, result) {
                            done();

                            if (err) {
                                return res.renderError(500, req, res);
                            }

                            project.response_messages = JSON.parse(project.response_messages);
                            
                            res.render('project/project.html', {
                                title: project.title,
                                user: req.user,
                                project: project,
                                users: result.rows,
                                edit_mode: true
                            });
                        }
                    );
                }
            );
        });
    };

    this.destroy = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query('SELECT * FROM project WHERE id = ($1)', [req.params.project_id], function(err, result) {
                if (err) {
                    done();
                    return res.renderError(500, req, res);
                }

                // Get the project
                var project = result.rows[0];
                if (!project) {
                    done();
                    return res.redirect('/project');
                }
                else if (project.owner_id != req.user.id) {
                    done();
                    return res.renderError(403, req, res);
                }

                // Got the project, let's delete it
                client.query('DELETE FROM project CASCADE WHERE id=($1)', [req.params.project_id], function(err, result) {
                    // Done working with the db client
                    done();

                    if (err) {
                        return res.renderError(500, req, res);
                    }

                    res.redirect('/project');
                });
            });
        });
    };

    this.addUserAccess = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            // Get the project with permissions first
            permissions.projectWithPermissions(
                req, res, client, done, req.user, req.params.project_id,
                function(project) {
                    // if the project is not owned by a user or the user has no admin permissions
                    if (project.owner_id != req.user.id && project.level != 3) {
                        done();

                        return res.renderError(403, req, res);
                    }
                    
                    // Adding the user into the permissions list
                    client.query(
                        'INSERT INTO permission (project_id, user_id, level)' +
                        'VALUES (($1), ($2), 1);',
                        [
                            req.params.project_id,
                            req.body.user_id
                        ],
                        function(err, result) {
                            done();
                            if (err) {
                                return res.renderError(500, req, res);
                            }

                            // the user has been successfully added to the permissions list
                            res.sendStatus(201);
                        }
                    );
                }
            );
        });
    };

    this.updateUserAccess = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            // Get the project with permissions first
            permissions.projectWithPermissions(
                req, res, client, done, req.user, req.params.project_id,
                function(project) {
                    // if the project is not owned by a user or the user has no admin permissions
                    if (project.owner_id != req.user.id && project.level != 3) {
                        done();

                        return res.renderError(403, req, res);
                    }
                    
                    // Adding the user into the permissions list
                    client.query(
                        'UPDATE permission SET level = ($3) WHERE user_id = ($2) AND project_id = ($1);',
                        [
                            req.params.project_id,
                            req.params.user_id,
                            req.body.level
                        ],
                        function(err, result) {
                            done();

                            if (err) {
                                return res.renderError(500, req, res);
                            }

                            // the user has been successfully updated in the permissions list
                            res.sendStatus(200);
                        }
                    );
                }
            );
        });
    };
    
    this.deleteUserAccess = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            // Get the project with permissions first
            permissions.projectWithPermissions(
                req, res, client, done, req.user, req.params.project_id,
                function(project) {
                    // if the project is not owned by a user or the user has no admin permissions
                    if (project.owner_id != req.user.id && project.level != 3) {
                        done();
                        return res.renderError(403, req, res);
                    }

                    // delete the user into the permissions list
                    client.query(
                        'DELETE FROM permission WHERE user_id = ($1) and project_id = ($2);',
                        [
                            req.params.user_id,
                            req.params.project_id
                        ],
                        function(err, result) {
                            done();

                            if (err) {
                                return res.renderError(500, req, res);
                            }

                            // the user has been deleted from the permissions table
                            res.sendStatus(200);
                        }
                    );
                }
            );
        });
    };
}


// Routes
var projectController = new ProjectController();
router.get('/', accessControl.noGuests, projectController.index);
router.post('/', accessControl.noGuests, projectController.create);
router.get('/new', accessControl.noGuests, projectController.new_form);
router.get('/:project_id', projectController.show);
router.post('/:project_id', accessControl.noGuests, projectController.update);
router.post('/:project_id/access/add', accessControl.noGuests, projectController.addUserAccess);
router.post('/:project_id/access/:user_id/update', accessControl.noGuests, projectController.updateUserAccess);
router.get('/:project_id/edit', accessControl.noGuests, projectController.edit);
router.get('/:project_id/delete', accessControl.noGuests, projectController.destroy);
router.get('/:project_id/access/:user_id/delete', accessControl.noGuests, projectController.deleteUserAccess);

module.exports = exports = router;
