var encryption = require('../db/encryption');
var db = require('../db/db');
var express = require('express');
var router = express.Router();


// A session controller for logging in and out users
function SessionController() {
    // Renders a login form with no error message
    this.new = function(req, res) {
        res.render('session/new.html', {user: req.user});
    };

    // Creates a new session, provided the username and password match one in the database,
    // If not, renders the login form with an error message.
    this.create = function(req, res, next) {
        req.session.reset();

        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query('SELECT * FROM api_user WHERE username = ($1)', [req.body.username], function (err, result) {
                // Done with the db client
                done();

                var user = result.rows[0];

                if (err) {
                    return res.render('session/new.html', {
                        message: 'Username/Password not found.  Please try again.',
                        user: req.user
                    });
                }

                if (!user) {
                    return res.render('session/new.html', {
                        message: 'Username/Password not found.  Please try again.',
                        user: req.user
                    });
                }

                if (user.password_digest != encryption.digest(req.body.password + user.salt)) {
                    return res.render('session/new.html', {
                        message: 'Username/Password not found.  Please try again.',
                        user: req.user
                    });
                }

                if (!user.is_activated) {
                    return res.render('session/new.html', {
                        message: 'You need to activate your account.  Please check your email and click the link provided.',
                        user: req.user
                    });
                }

                req.session.user_id = user.id;

                return res.redirect('/project');
            });
        });
    };

    // Ends a user session by flushing the session cookie.
    this.destroy = function(req, res) {
        req.session.reset();
        res.render('session/delete.html', {user: {username: 'Guest'}});
    }
}


// Routes
var sessionController = new SessionController();

// Index page route
router.get('/', function(req, res) {
    // Has registration, forward to the projects list
    if (req.user && req.user.username != 'Guest') {
        return res.redirect('/project');
    }

    // No registration, show landing
    req.session.reset();
    res.render('layout/landing.html', {user: {username: 'Guest'}});
});

// Documentation page route
router.get('/documentation', function(req, res) {
    res.render('layout/documentation.html', {user: req.user});
});

router.get('/login', sessionController.new);
router.post('/login', sessionController.create);
router.get('/logout', sessionController.destroy);


module.exports = exports = router;
