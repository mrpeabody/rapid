var db = require('../db/db');
var accessControl = require('../middleware/access_control');
var marked = require("marked");
var express = require('express');
var router = express.Router();
var projectWithPermissions = require('./../middleware/permissions').projectPermissions;


// A page controller for handling project pages (how-to, quick-start, etc) (create/list/delete/etc)
function PageController() {
    var loadSideMenuItems = function(req, res, client, done, projectId, callback) {
        // Query pages and modules
        client.query(
            'SELECT m.*, p.owner_id from module AS m JOIN project AS p ON p.id = project_id ' +
            'WHERE project_id = ($1) ORDER BY rank ASC, title ASC', [projectId],
            function(err, result) {
                if (err) {
                    done();

                    return res.renderError(500, req, res);
                }

                // save modules
                var modules = result.rows.filter(function(module) {
                    return module.owner_id == req.session.user_id || !module.is_private;
                });

                client.query(
                    'SELECT pg.*, p.owner_id from page AS pg JOIN project AS p ON p.id = project_id ' +
                    'WHERE project_id = ($1) ORDER BY rank ASC, title ASC', [projectId],
                    function(err, result) {
                        // Done working with the db client
                        done();

                        if (err) {
                            return res.renderError(500, req, res);
                        }

                        // save pages
                        var pages = result.rows.filter(function(page) {
                            return page.owner_id == req.session.user_id || !page.is_private;
                        });

                        // Respond to the callback
                        callback(modules, pages);
                    }
                );
            }
        );
    };
    
    this.new = function(req, res) {
        res.render('page/page.html', {user: req.user, project_id: req.params.project_id});
    };

    this.create = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // Get the project and verify that the user can create a page
            client.query('SELECT * FROM project WHERE id = ($1)', [req.params.project_id], function(err, result) {
                var project = result.rows[0];

                if (err || !project) {
                    done();
                    
                    return res.renderError(500, req, res);
                }

                if (project.owner_id != req.user.id) {
                    done();
                    
                    return res.renderError(403, req, res);
                }

                // Everything is fine, now we can actually store the page
                client.query(
                    'INSERT INTO page (title, content, rank, is_private, project_id) ' +
                    'VALUES (($1),($2),($3),($4),($5)) RETURNING id;', [
                        req.body.title && req.body.title.length ? req.body.title : null,
                        req.body.content,
                        req.body.rank,
                        req.body.is_private ? true : false,
                        req.params.project_id
                    ],
                    function(err, result) {
                        // Done working with the db client
                        done();

                        if (err) {
                            return res.render('page/page.html', {
                                project_id: project.id,
                                user: req.user,
                                page: req.body,
                                message: 'Please check the entered information again, then submit'
                            });
                        }

                        res.redirect('/page/' + result.rows[0].id);
                    }
                );
            });
        });
    };

    this.show = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // Everything is fine, now we can actually return the page content to show
            client.query('SELECT * FROM page WHERE id = ($1)', [req.params.page_id], function(err, result) {
                if (err) {
                    done();
                    
                    return res.renderError(500, req, res);
                }

                // Get the page
                var page = result.rows[0];
                if (!page) {
                    done();
                    
                    return res.renderError(404, req, res);
                }

                // Get the project and verify that the user can create a page
                client.query('SELECT * FROM project WHERE id = ($1)', [page.project_id], function(err, result) {
                    var project = result.rows[0];

                    if (err || !project) {
                        done();
                        
                        return res.renderError(500, req, res);
                    }

                    if ((project.is_private || page.is_private) && project.owner_id != req.user.id) {
                        done();
                        
                        return res.renderError(403, req, res);
                    }
                    
                    // Load modules and pages
                    loadSideMenuItems(req, res, client, done, project.id, function(modules, pages) {
                        // Parse the content of the page
                        page.content = marked(page.content);

                        var returnDict = {
                            title: page.title,
                            user: req.user,
                            owner_id: project.owner_id,
                            project_id: project.id,
                            project: project,
                            page: page,
                            modules: modules,
                            pages: pages
                        }

                        if (req.query.mode == 'json') {
                            res.json(returnDict);
                        }
                        else {
                            res.render('page/show.html', returnDict);
                        }
                    });
                });
            });
        });
    };

    this.edit = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // Everything is fine, now we can actually return the page content to edit
            client.query('SELECT * FROM page WHERE id = ($1)', [req.params.page_id], function(err, result) {
                if (err) {
                    done();
                    
                    return res.renderError(500, req, res);
                }

                // Get the page
                var page = result.rows[0];
                if (!page) {
                    done();
                    
                    return res.renderError(404, req, res);
                }

                // Get the project and verify that the user can create a page
                client.query('SELECT * FROM project WHERE id = ($1)', [page.project_id], function(err, result) {
                    // Done working with the db client
                    done();

                    var project = result.rows[0];

                    if (err || !project) {
                        return res.renderError(500, req, res);
                    }

                    if (project.owner_id != req.user.id) {
                        return res.renderError(403, req, res);
                    }

                    res.render('page/page.html', {
                        title: page.title,
                        user: req.user,
                        page: page,
                        edit_mode: true
                    });
                });
            });
        });
    };

    this.update = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            // Get the project and verify that the user can update the page
            client.query(
                'SELECT pg.id, pg.project_id, p.owner_id FROM page AS pg ' +
                'JOIN project AS p ON p.id = pg.project_id WHERE pg.id = ($1)',
                [req.params.page_id],
                function(err, result) {
                    var queryResult = result.rows[0];

                    if (err || !queryResult) {
                        done();
                        
                        return res.renderError(500, req, res);
                    }

                    if (queryResult.owner_id != req.user.id) {
                        done();
                        
                        return res.renderError(403, req, res);
                    }

                    // Everything is fine, now we can actually update the page
                    client.query(
                        'UPDATE page SET title = ($1), content = ($2), rank = ($3), ' +
                        'is_private = ($4) WHERE id = ($5)', [
                            req.body.title && req.body.title.length ? req.body.title : null,
                            req.body.content,
                            req.body.rank,
                            req.body.is_private ? true : false,
                            req.params.page_id
                        ],
                        function(err, result) {
                            // Done working with the db client
                            done();

                            if (err) {
                                return res.render('page/page.html', {
                                    user: req.user,
                                    page: req.body,
                                    edit_mode: true,
                                    message: 'Please check the entered information again, then submit'
                                });
                            }

                            res.redirect('/page/' + queryResult.id);
                        }
                    );
                }
            );
        });
    };

    this.destroy = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query(
                'SELECT pg.id, p.owner_id, pg.project_id FROM page AS pg ' +
                'JOIN project AS p ON p.id = pg.project_id WHERE pg.id = ($1)',
                [req.params.page_id],
                function(err, result) {
                    if (err) {
                        done();
                        
                        return res.renderError(500, req, res);
                    }

                    // Get the queryResult
                    var queryResult = result.rows[0];
                    if (!queryResult) {
                        done();
                        
                        return res.redirect('/project');
                    }
                    else if (queryResult.owner_id != req.user.id) {
                        done();
                        
                        return res.renderError(403, req, res);
                    }

                    // Got the page, let's delete it
                    client.query('DELETE FROM page WHERE id=($1)', [req.params.page_id], function(err, result) {
                        // Done working with the db client
                        done();

                        if (err) {
                            return res.renderError(500, req, res);
                        }

                        res.redirect('/project/' + queryResult.project_id);
                    });
                }
            );
        });
    };
}


// Routes
var pageController = new PageController();
router.get('/:page_id', pageController.show);
router.post('/:page_id', accessControl.noGuests, pageController.update);
router.get('/:page_id/edit', accessControl.noGuests, pageController.edit);
router.get('/:page_id/delete', accessControl.noGuests, pageController.destroy);
router.post('/project/:project_id', accessControl.noGuests, pageController.create);
router.get('/project/:project_id/new', accessControl.noGuests, pageController.new);


module.exports = exports = router;
