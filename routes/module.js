var db = require('../db/db');
var accessControl = require('../middleware/access_control');
var marked = require('marked');
var express = require('express');
var router = express.Router();


// A module controller for handling project modules (API methods lists) (create/list/delete/etc)
function ModuleController() {
    var loadModuleDataItems = function(req, res, client, done, projectId, moduleId, callback) {
        // Query pages and modules
        client.query(
            'SELECT m.*, p.owner_id from module AS m JOIN project AS p ON p.id = project_id ' +
            'WHERE project_id = ($1) ORDER BY rank ASC, title ASC', [projectId],
            function(err, result) {
                if (err) {
                    done();
                    return res.renderError(500, req, res);
                }

                // save modules
                var modules = result.rows.filter(function(module) {
                    return module.owner_id == req.session.user_id || !module.is_private;
                });

                client.query(
                    'SELECT pg.*, p.owner_id from page AS pg JOIN project AS p ON p.id = project_id ' +
                    'WHERE project_id = ($1) ORDER BY rank ASC, title ASC', [projectId],
                    function(err, result) {
                        if (err) {
                            done();

                            return res.renderError(500, req, res);
                        }

                        // save pages
                        var pages = result.rows.filter(function(page) {
                            return page.owner_id == req.session.user_id || !page.is_private;
                        });

                        client.query('SELECT * from method WHERE module_id = ($1) ORDER BY rank ASC', [moduleId],
                            function(err, result) {
                                // Done working with the db client
                                done();

                                if (err) {
                                    return res.renderError(500, req, res);
                                }

                                // Sorting function for pages
                                var itemSort = function(a, b) {
                                    return a.rank - b.rank 
                                        || (a.url_name > b.url_name ? 1 : (a.url_name < b.url_name ? -1 : 0))
                                        || a.date_created - b.date_created;
                                };

                                // Respond to the callback
                                callback(modules, pages, result.rows.sort(itemSort));
                            });
                    }
                );
            }
        );
    };

    this.new = function(req, res) {
        res.render('module/module.html', {
            user: req.user,
            project_id: req.params.project_id
        });
    };

    this.create = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            // Get the project and verify that the user can create a module
            client.query('SELECT * FROM project WHERE id = ($1)', [req.params.project_id], function(err, result) {
                var project = result.rows[0];

                if (err || !project) {
                    done();

                    return res.renderError(500, req, res);
                }

                if (project.owner_id != req.user.id) {
                    done();

                    return res.renderError(403, req, res);
                }

                // Everything is fine, now we can actually store the module
                client.query(
                    'INSERT INTO module (title, description, rank, is_private, project_id) ' +
                    'VALUES (($1),($2),($3),($4),($5)) RETURNING id;', [
                        req.body.title && req.body.title.length ? req.body.title : null,
                        req.body.description,
                        req.body.rank,
                        req.body.is_private ? true : false,
                        req.params.project_id
                    ],
                    function(err, result) {
                        // Done working with the db client
                        done();

                        if (err) {
                            return res.render('module/module.html', {
                                project_id: project.id,
                                user: req.user,
                                module: req.body,
                                message: 'Please check the entered information again, then submit'
                            });
                        }

                        res.redirect('/module/' + result.rows[0].id);
                    }
                );
            });
        });
    };

    this.show = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            // Everything is fine, now we can actually return the module data to show
            client.query('SELECT * FROM module WHERE id = ($1)', [req.params.module_id], function(err, result) {
                if (err) {
                    done();

                    return res.renderError(500, req, res);
                }

                // Get the module
                var module = result.rows[0];
                if (!module) {
                    done();

                    return res.renderError(404, req, res);
                }

                // Get the project and verify that the user can create a module
                client.query('SELECT * FROM project WHERE id = ($1)', [module.project_id], function(err, result) {
                    var project = result.rows[0];

                    if (err || !project) {
                        done();

                        return res.renderError(500, req, res);
                    }

                    if ((project.is_private || module.is_private) && project.owner_id != req.user.id) {
                        done();

                        return res.renderError(403, req, res);
                    }

                    // Sorting function for pages
                    var itemSort = function(a, b) {
                        return a.rank - b.rank ||
                            (a.url_name > b.url_name ? 1 : (a.url_name < b.url_name ? -1 : 0)) ||
                            a.date_created - b.date_created;
                    };

                    // Load modules and pges
                    loadModuleDataItems(req, res, client, done, project.id, module.id, function(modules, pages, methods) {
                        // Parse the description of the module
                        module.description = marked(module.description);

                        var returnDict = {
                            title: module.title,
                            user: req.user,
                            project: project,
                            module: module,
                            owner_id: project.owner_id,
                            project_id: module.project_id,
                            modules: modules,
                            pages: pages,
                            methods: methods.filter(function(method) {
                                return project.owner_id == req.user.id || !method.is_private;
                            }).sort(itemSort)
                        }

                        if (req.query.mode == 'json') {
                            res.json(returnDict);
                        }
                        else {
                            res.render('module/show.html', returnDict);
                        }
                    });
                });
            });
        });
    };

    this.edit = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            // Everything is fine, now we can actually return the module data to edit
            client.query('SELECT * FROM module WHERE id = ($1)', [req.params.module_id], function(err, result) {
                if (err) {
                    done();

                    return res.renderError(500, req, res);
                }

                // Get the module
                var module = result.rows[0];
                if (!module) {
                    done();

                    return res.renderError(404, req, res);
                }

                // Get the project and verify that the user can create a module
                client.query('SELECT * FROM project WHERE id = ($1)', [module.project_id], function(err, result) {
                    // Done working with the db client
                    done();

                    var project = result.rows[0];

                    if (err || !project) {
                        return res.renderError(500, req, res);
                    }

                    if (project.owner_id != req.user.id) {
                        return res.renderError(403, req, res);
                    }

                    res.render('module/module.html', {
                        title: module.title,
                        user: req.user,
                        module: module,
                        edit_mode: true
                    });
                });
            });
        });
    };

    this.update = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            // Get the project and verify that the user can update the module
            client.query(
                'SELECT m.id, m.project_id, p.owner_id FROM module AS m ' +
                'JOIN project AS p ON p.id = m.project_id WHERE m.id = ($1)', [req.params.module_id],
                function(err, result) {
                    var queryResult = result.rows[0];

                    if (err || !queryResult) {
                        done();

                        return res.renderError(500, req, res);
                    }

                    if (queryResult.owner_id != req.user.id) {
                        done();

                        return res.renderError(403, req, res);
                    }

                    // Everything is fine, now we can actually update the module
                    client.query(
                        'UPDATE module SET title = ($1), description = ($2), rank = ($3), ' +
                        'is_private = ($4) WHERE id = ($5)', [
                            req.body.title && req.body.title.length ? req.body.title : null,
                            req.body.description,
                            req.body.rank,
                            req.body.is_private ? true : false,
                            req.params.module_id
                        ],
                        function(err, result) {
                            // Done working with the db client
                            done();

                            if (err) {
                                return res.render('module/module.html', {
                                    user: req.user,
                                    module: req.body,
                                    edit_mode: true,
                                    message: 'Please check the entered information again, then submit'
                                });
                            }

                            res.redirect('/module/' + queryResult.id);
                        }
                    );
                }
            );
        });
    };

    this.destroy = function(req, res) {
        db.connect(function(err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }

            client.query(
                'SELECT m.id, p.owner_id, m.project_id FROM module AS m ' +
                'JOIN project AS p ON p.id = m.project_id WHERE m.id = ($1)', [req.params.module_id],
                function(err, result) {
                    if (err) {
                        done();

                        return res.renderError(500, req, res);
                    }

                    // Get the queryResult
                    var queryResult = result.rows[0];
                    if (!queryResult) {
                        done();

                        return res.redirect('/project');
                    } else if (queryResult.owner_id != req.user.id) {
                        done();

                        return res.renderError(403, req, res);
                    }

                    // Got the module, let's delete it
                    client.query('DELETE FROM module CASCADE WHERE id=($1)', [req.params.module_id], function(err, result) {
                        // Done working with the db client
                        done();

                        if (err) {
                            return res.renderError(500, req, res);
                        }

                        res.redirect('/project/' + queryResult.project_id);
                    });
                }
            );
        });
    };
}


// Routes
var moduleController = new ModuleController();
router.get('/:module_id', moduleController.show);
router.post('/:module_id', accessControl.noGuests, moduleController.update);
router.get('/:module_id/edit', accessControl.noGuests, moduleController.edit);
router.get('/:module_id/delete', accessControl.noGuests, moduleController.destroy);
router.post('/project/:project_id', accessControl.noGuests, moduleController.create);
router.get('/project/:project_id/new', accessControl.noGuests, moduleController.new);


module.exports = exports = router;
