var currentUser;


$(document).ready(function() {
    // User search related code
    $('#email-search').on('input', userSearch);

    $('#clearButton').click(function() {
        var userInput = $('#userSearch');
        userInput.val("");
        userInput.focus();
    });


    /**
     * 
     *
     * @returns {undefined}
     */
    $('#user-add').on('click', function() {
        var projectId = $(this).attr('project-id');

        // short circuit in case of no user selected
        if (!currentUser) {
            return;
        }

        // comment here
        $.ajax({
            url: '/project/' + projectId + '/access/add',
            type: 'POST',
            data: {
                user_id: currentUser.id
            },
            error: function(err) {
                console.log('err: ', err);
            },
            success: function(result) {
                console.log('result: ', result);

                // add user to table, later
                $('#permission-table').append(
                    '<tr user-id="' + currentUser.id + '">' + 
                    '<th scope="row" class="project-row">' + 5 + '</th>' + 
                    '<td class="project-row">' + currentUser.username + '</td>' + 
                    '<td class="project-row">' + currentUser.first_name + '</td>' + 
                    '<td>' + 
                    '<div class="btn-group btn-group-xs" role="group">' + 
                    '<button type="button" level="1" class="btn btn-default permission-level active">Read</button>' + 
                    '<button type="button" level="2" class="btn btn-default permission-level">Write</button>' + 
                    '<button type="button" level="3" class="btn btn-default permission-level">Admin</button>' + 
                    '</div>' + 
                    '</td>' + 
                    '<td>' + 
                    '<a class="delete-link" delete-link="/project/' + projectId + '/access/' + currentUser.id + '/delete">' + 
                    '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>' + 
                    '</td>'
                );

                // clear the search bar
                $('#email-search').val('');
            }
        });
    });

    $(document).on('click', '.delete-link', function() {
        var deleteLink = $(this);
        $.ajax({
            url: deleteLink.attr('delete-link'),
            type: 'GET',
            error: function(err) {
                console.log('err: ', err);
            },
            success: function(result) {
                console.log('result: ', result);
                deleteLink.parent().parent().remove();
            }
        });
    });

    $(document).on('click', '.permission-level', function() {
        var button = $(this);

        // return out if they hit a current permission
        if (button.hasClass('active')) {
            return;
        }
        
        var parent = button.parent();
        var userId = button.parent().parent().parent().attr('user-id');
        
        $.ajax({
            url: '/project/' + $('#user-add').attr('project-id') + '/access/' + userId + '/update',
            type: 'POST',
            data: {
                level: button.attr('level')
            },
            error: function(err) {
                console.log('err: ', err);
            },
            success: function(result) {
                parent.children('.active').removeClass('active');
                button.addClass('active');
            }
        });
    });

    // Prevent submit on enter
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});


function userSearch() {
    // get the length and only call the server if the input is greater then 5
    var email = $('#email-search').val();
    if (email.length < 4) {
        $('#foundUserMenuToggle').removeClass('open');
        return;
    }

    // get the user objects
    loadSuggestedUsers(email, function(users) {
        console.log('users: ', users);
        // purge previous values
        var menu = $('#suggestedUsers');

        if (users.length > 0) {
            for (var i = 0; i < users.length; ++i) {
                var foundUserLink = $('<a class="found-user-link" user-id="'
                    + users[i].id
                        + "\" user-last='"
                        + users[i].last_name
                        + "' user-first='"
                        + users[i].first_name
                        + "' user-email='"
                        + users[i].username
                        + "' href='#'>"
                        + '<b>'
                        + users[i].username
                        + '</b>'
                        + ( users[i].first_name !== null ? " " + users[i].first_name : "")
                        + ( users[i].last_name !== null ? " " + users[i].last_name : "")
                        + '</a>');

                // assign the on-click handler
                (function(thisUser) {
                    foundUserLink.click(function() {
                        // Hide the user list
                        $('#foundUserMenuToggle').removeClass('open');

                        // Assign the current user and show the user in the input
                        currentUser = thisUser;
                        $('#email-search').val(thisUser.username);
                    });
                })(users[i]);

                // add the link to the menu
                var wrappedLink = $('<li></li>').append(foundUserLink);
                wrappedLink.appendTo(menu);
            }

            // show menu
            $('#foundUserMenuToggle').addClass('open');
        }
        else {
            $('#foundUserMenuToggle').removeClass('open');
        }
    });
}


function loadSuggestedUsers(inputString, callback) {
    $("#suggestedUsers").children().remove();

    // call the server and get a list of names for the user
    $.ajax({
        url: '/user/search/',
        data: {
            search_query: inputString
        },
        type: 'POST',
        success: function(result) {
            // return the list of users found
            callback(result);
        },
        error: function(err) {
            console.log('err: ', err);
            callback([]);
        }
    });
}

