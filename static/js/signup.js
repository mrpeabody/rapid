$(document).ready(function() {
    // Assign input validators
    $('input[name="username"]').on('input', usernameIsValid);
    $('input[name="password"]').on('input', passwordIsValid);
    $('input[name="password_confirm"]').on('input', passwordsMatch);
    $('#form-button').click(submitButtonPressed);

    // Detecting the edit mode
    var oldPassword = $('input[name="old_password"]');
    if (oldPassword.length) {
        oldPassword.on('input', oldPasswordIsValid);
    }
});


/**
 * Validates username
 */
function usernameIsValid() {
    var username = $('input[name="username"]').val();
    var usernameGroup = $('#username-form-group').removeClass('has-success has-error');
    var usernameHelpText = $('#username-help-block');

    // Short password or contains restricted symbols
    if (username.length < 8 || !username.match(/^[0-9A-Za-z#$&-.@_]+$/)) {
        usernameHelpText.text('Username should be at least 8 symbols long. Special symbols allowed: # $ & - . @ _');
        usernameGroup.addClass('has-error');

        return false;
    }

    // Everything is fine with the password field itself - set green border
    usernameHelpText.text('Username is valid');
    usernameGroup.addClass('has-success');

    return true;
}


/**
 * Validates the password field
 * @returns {boolean}
 */
function passwordIsValid() {
    var password = $('input[name="password"]').val();
    var passwordGroup = $('#password-form-group').removeClass('has-success has-error');
    var passwordHelpText = $('#password-help-block');

    // Passwords have to match
    passwordsMatch();

    // Short password or contains restricted symbols
    var fail = password.length < 8 || !password.match(/^[0-9A-Za-z#$&-.@_]+$/);
    if (fail) {
        passwordHelpText.text('Password should be at least 8 symbols long. Special symbols allowed: # $ & - . @ _');
        passwordGroup.addClass('has-error');

        return false;
    }

    // Everything is fine with the password field itself - set green border
    passwordHelpText.text('');
    passwordGroup.addClass('has-success');

    return true;
}


/**
 * Validates that passwords match.
 * @returns {boolean}
 */
function passwordsMatch() {
    var passwordConfirmGroup = $('#password-confirm-form-group').removeClass('has-success has-error');
    var passwordConfirmHelpText = $('#password-confirm-help-block');
    var passwordConfirmText = $('input[name="password_confirm"]').val();

    var result = $('input[name="password"]').val() == passwordConfirmText && passwordConfirmText.length > 0;
    passwordConfirmGroup.addClass(result ? 'has-success' : 'has-error');
    passwordConfirmHelpText.text(result ? 'Passwords match' : 'Passwords do not match');

    return result;
}


/**
 * Validates old password (if any)
 */
function oldPasswordIsValid() {
    var password = $('input[name="old_password"]').val();
    var passwordGroup = $('#old-password-form-group').removeClass('has-success has-error');
    var passwordHelpText = $('#old-password-help-block');

    // Short password or contains restricted symbols
    var fail = password.length < 8 || !password.match(/^[0-9A-Za-z#$&-.@_]+$/);
    if (fail) {
        passwordHelpText.text('Password should be at least 8 symbols long. Special symbols allowed: # $ & - . @ _');
        passwordGroup.addClass('has-error');

        return false;
    }

    // Everything is fine with the password field itself - set green border
    passwordHelpText.text('');
    passwordGroup.addClass('has-success');

    return true;
}


/**
 * This function submits the form, or show an error label if something is broken
 * @returns {*|jQuery}
 */
function submitButtonPressed() {
    // Also optionally check for the old password
    var oldPassword = $('input[name="old_password"]');
    var checkForOldPassword = oldPassword.length > 0;

    // Scenario 1 - checking signup form
    if (!checkForOldPassword) {
        if (passwordIsValid() && passwordsMatch() && usernameIsValid()) {
            return $('#user-form').submit();
        }
    }
    // Scenario 2 - Update profile
    else {
        // if we have something in the password field or oldPassword field, need to validate all three
        var oldPasswordValue = oldPassword.val();
        var passwordValue = $('input[name="password"]').val();

        var valid = true;
        if (oldPasswordValue.length || passwordValue.length) {
            valid = passwordIsValid() && passwordsMatch() && oldPasswordIsValid();
        }

        if (valid) {
            return $('#user-form').submit();
        }
    }

    // Error found
    var messageLabel = $('#message-label');
    messageLabel.removeClass('alert-success alert-danger');
    messageLabel.text('Please validate your data and hit Submit again').addClass('alert-danger').show();
}