$(document).ready(function() {
    // on submit click
    $('#submit').click(sendProjectData);

    // on up arrow click
    $(document).on('click', '.glyphicon-arrow-up', upArrowClicked);
    
    // on down arrow click
    $(document).on('click', '.glyphicon-arrow-down', downArrowClicked);

    // set up the messages table
    setupMessagesTable();
});


function sendProjectData() {
    // hide the error label
    $('#alert-label').hide();

    // collect the data
    var data = {
        title: $('input[name=title]').val(),
        url: $('input[name=url]').val(),
        is_private: $('input[name=is_private]').is(':checked') === true,
        description: $('textarea[name=description]').val(),
        response_messages: messagesToJSON()
    };

    // validate fields
    if (dataIsValid(data)) {
        $.ajax({
            url: $('#project-form').attr('action'),
            type: 'POST',
            data: {project: JSON.stringify(data)},
            success: function(response) {
                // redirect to SHOW page
                window.location.href = response.url;
            },
            error: function(err) {
                // Error happened, show it in the error label
                $('#alert-label').text('Error ' + err.status + ': ' + err.statusText).show();
            }
        });
    }
}


/**
 * This function checks the data collected from the form and ensures all required fields are there
 * @param data
 */
function dataIsValid(data) {
    var errorMessage = '';

    if (!data.title) errorMessage += 'Title must be entered<br>';
    if (!data.description) errorMessage += 'Description must be entered<br>';

    // populate the error div if we found something
    if (errorMessage) {
        $('#alert-label').html(errorMessage).show();

        return false;
    }

    return true;
}


// Functions that handle the messages table
function setupMessagesTable() {
    // parameter links
    $('#add-message-link').on('click', insertMessageTable);
    $('#first-message-link').on('click', insertMessageTable);
    $(document).on('click', '.delete-message', removeMessageRow);
}


function messagesToJSON() {
    var messages = [];
    var messageRows = $('#message-table-body').children();

    for (var i = 0; i < messageRows.length; ++i) {
        var row = messageRows.eq(i);

        // Extract and save data
        var code = row.find('#message-code').text();
        var name = row.find('#message-name').text();
        var description = row.find('#message-description').text();

        // if there's no name, cannot send this
        if (!name || !code) {
            continue;
        }

        messages.push({
            code: code,
            name: name,
            description: description
        });
    }

    return JSON.stringify(messages);
}


function upArrowClicked() {
    var row = $(this).closest('tr');
    row.insertBefore(row.prev());
}


function downArrowClicked() {
    var row = $(this).closest('tr');
    row.insertAfter(row.next());
}


function insertMessageTable() {
    // hide of the empty-table label
    $('#empty-messages-label').hide();

    // get the messages panel body
    var messagePanel = $('#message-panel');
    if (!messagePanel.has($('#message-table')).length) {
        // Create a table
        var table = $(
            '<table id="message-table" class="table table-default">' +
            '    <thead>' +
            '    <tr>' +
            '        <th>Code</th>' +
            '        <th>Name</th>' +
            '        <th>Description</th>' +
            '        <th></th>' +
            '    </tr>' +
            '    </thead>' +
            '    <tbody id="message-table-body"></tbody>' +
            '</table>'
        ).appendTo(messagePanel);
    }

    // Now we also should insert the empty row
    addMessageRow();
}


/**
 * This function inserts a so called 'new row'to the end of the table
 */
function addMessageRow() {
    // insert the new row and set the first field in the row in focus
    generateMessageRow().appendTo($('#message-table-body')).find('#message-code').focus();
}


/**
 * This function generates the DOM node that contains the new message row. Can also be holing values passed as arguments
 */
function generateMessageRow(code, name, description) {
    return $(
        '<tr id="new-message-row">' +
        '    <td>' +
        '        <div id="message-code" contenteditable="true" class="input-cell">' +
        (code != undefined ? code : '') + '</div>' +
        '    </td>' +
        '    <td>' +
        '        <div id="message-name" contenteditable="true" class="input-cell">' +
        (name ? name : '') + '</div>' +
        '    </td>' +
        '    <td>' +
        '        <div id="message-description" contenteditable="true" class="input-cell">' +
        (description ? description : '') + '</div>' +
        '    </td>' +
        '    <td>' +
        '        <span class="panel-controls pull-right">' +
                    '<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>' +
                    '<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>' +
                    '&nbsp;' +
        '            <a class="panel-link delete-message">' +
        '                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>' +
        '            </a>' +
        '        </span>' +
        '    </td>' +
        '</tr>'
    );
}


/**
 * This function remove a message row
 */
function removeMessageRow() {
    var row = $(this).parent().parent().parent();

    // remove the row itself
    row.remove();

    // check if there are no more rows, show the empty label in that case
    if ($('#message-table-body').children().length == 0) {
        // delete the table
        $('#message-table').remove();

        // show of the empty-table label
        $('#empty-messages-label').show();
    }
}

