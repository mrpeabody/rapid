$(document).ready(function() {
    // on submit click
    $('#submit').click(sendMethodData);

    // Show/Hide description
    $('.spoiler-trigger').click(function() {
        $(this).parent().next().collapse('toggle');
    });

    // on response type change
    $('.response-menu-item').click(function() {
        // The selected option becomes a new request button title
        $('#response-type-value').text($(this).text());
    });

    // on request type change
    $('.request-menu-item').click(function() {
        // The selected option becomes a new request button title
        $('#request-type-value').text($(this).text());

        // adjust background and the button color
        changeFormBackground();
    });

    // on up arrow click
    $(document).on('click', '.glyphicon-arrow-up', upArrowClicked);
    
    // on down arrow click
    $(document).on('click', '.glyphicon-arrow-down', downArrowClicked);

    $(document).on('change', '.response-message-checkbox', responseSelectionChange);

    setupParametersTable();

    // set up the messages table
    setupMessagesTable();

    // initial setup when loading the method new/edit page
    changeFormBackground();
});



/**
 * This function uploads the form data to the server, creating/updating the method
 */
function sendMethodData() {
    // hide the error label
    $('#alert-label').hide();

    // collect the data
    var data = {
        url_name: $('input[name=url_name]').val(),
        rank: $('input[name=rank]').val(),
        short_description: $('input[name=short_description]').val(),
        is_private: $('input[name=is_private]').is(':checked') === true,
        full_description: $('textarea[name=full_description]').val(),
        request_type: $('#request-type-value').text(),
        response_type: $('#response-type-value').text(),
        response: $('textarea[name=response]').val(),
        parameters: parametersToJSON(),
        project_codes: projectCodesToJSON(),
        response_messages: messagesToJSON()
    };

    // validate fields
    if (dataIsValid(data)) {
        $.ajax({
            url: $('#method-form').attr('action'),
            type: 'POST',
            data: {method: JSON.stringify(data)},
            success: function(response) {
                // redirect to SHOW page
                window.location.href = response.url;
            },
            error: function(err) {
                // Error happened, show it in the error label
                $('#alert-label').text('Error ' + err.status + ': ' + err.statusText).show();
            }
        });
    }
}


/**
 * This function checks the data collected from the form and ensures all required fields are there
 * @param data
 */
function dataIsValid(data) {
    var errorMessage = '';

    if (!data.url_name) errorMessage += 'URL Name must be entered<br>';
    if (!data.short_description) errorMessage += 'Short Description must be entered<br>';

    // populate the error div if we found something
    if (errorMessage) {
        $('#alert-label').html(errorMessage).show();

        return false;
    }

    return true;
}


// This function changes the background of the new/edit form according to the request type
function changeFormBackground() {
    // get required variables
    var form = $('#method-form');
    var requestButton = $('#request-button');
    var requestType = $('#request-type-value').text();

    // Clear previous style settings
    form.removeClass();
    requestButton.removeClass('btn-primary btn-success btn-warning btn-danger');

    // Now let's colorize stuff
    switch (requestType) {
        case 'GET':
            form.addClass('get');
            requestButton.addClass('btn-primary');
            break;

        case 'POST':
            form.addClass('post');
            requestButton.addClass('btn-success');
            break;

        case 'PUT':
            form.addClass('put');
            requestButton.addClass('btn-warning');
            break;

        case 'DELETE':
            form.addClass('delete');
            requestButton.addClass('btn-danger');
            break;

        default:
            // white page with no formatting, no color for the button
            requestButton.addClass('btn-default');
            break;
    }
}


// Functions that handle the parameters table
function setupParametersTable() {
    // parameter links
    $('#add-parameter-link').on('click', insertParameterTable);
    $('#first-parameter-link').on('click', insertParameterTable);
    $(document).on('click', '.delete-parameter', removeParameterRow);

    // on parameter location change
    $(document).on('click', '.parameter-location-item', function() {
        // The selected option becomes a new parameter location button title
        $(this).parents('ul').siblings('button').find('#parameter-location').text($(this).text());
    });

    // on parameter type change
    $(document).on('click', '.parameter-type-item', function() {
        // The selected option becomes a new parameter type button title
        $(this).parents('ul').siblings('button').find('#parameter-type').text($(this).text());
    });
}


/**
 * This function inserts an empty parameters table
 */
function insertParameterTable() {
    // hide of the empty-table label
    $('#empty-parameters-label').hide();

    // get the parameters panel body
    var parameterPanel = $('#parameter-panel');
    if (!parameterPanel.has($('#parameter-table')).length) {
        // Create a table
        var table = $(
            '<table id="parameter-table" class="table table-default">' +
            '    <thead>' +
            '    <tr>' +
            '        <th>Name</th>' +
            '        <th>Location</th>' +
            '        <th>Type</th>' +
            '        <th>Required</th>' +
            '        <th>Description</th>' +
            '        <th></th>' +
            '    </tr>' +
            '    </thead>' +
            '    <tbody id="parameter-table-body"></tbody>' +
            '</table>'
        ).appendTo(parameterPanel);
    }

    // Now we also should insert the empty row
    addParameterRow();
}


/**
 * This function inserts a so called 'new row'to the end of the table
 */
function addParameterRow() {
    // Get the table body and check if we already inserted the new row
    var parameterNameSelector = '#parameter-name';
    var parameterTableBody = $('#parameter-table-body');

    // insert the new row and et the first field in the row in focus
    generateParameterRow().appendTo(parameterTableBody).find(parameterNameSelector).focus();
}


/**
 * This function generates the DOM node that contains the new row. Can also be holing values passed as arguments
 */
function generateParameterRow(name, location, type, required, description) {
    return $(
        '<tr id="new-parameter-row">' +
        '    <td>' +
        '        <div id="parameter-name" contenteditable="true" class="input-cell">' +
        (name ? name : '') + '</div>' +
        '    </td>' +
        '    <td>' +
        '       <div class="btn-group">' +
        '       <button type="button" class="btn btn-default btn-xs dropdown-toggle"' +
        '               data-toggle="dropdown"' +
        '               aria-haspopup="true"' +
        '               aria-expanded="false">' +
        '           <span id="parameter-location">' +
        (location ? location : 'header') + '</span> <span class="caret"></span>' +
        '       </button>' +
        '       <ul class="dropdown-menu">' +
        '           <li><a class="parameter-location-item">header</a></li>' +
        '           <li><a class="parameter-location-item">path</a></li>' +
        '           <li><a class="parameter-location-item">body</a></li>' +
        '           <li><a class="parameter-location-item">file</a></li>' +
        '       </ul>' +
        '    </div>' +
        '    </td>' +
        '    <td>' +
        '       <div class="btn-group">' +
        '       <button type="button" class="btn btn-default btn-xs dropdown-toggle"' +
        '               data-toggle="dropdown"' +
        '               aria-haspopup="true"' +
        '               aria-expanded="false">' +
        '           <span id="parameter-type">' +
        (type ? type : 'string') + '</span> <span class="caret"></span>' +
        '       </button>' +
        '       <ul class="dropdown-menu">' +
        '           <li><a class="parameter-type-item">string</a></li>' +
        '           <li><a class="parameter-type-item">integer</a></li>' +
        '           <li><a class="parameter-type-item">float</a></li>' +
        '           <li><a class="parameter-type-item">boolean</a></li>' +
        '           <li><a class="parameter-type-item">binary</a></li>' +
        '           <li><a class="parameter-type-item">array</a></li>' +
        '       </ul>' +
        '    </div>' +
        '    </td>' +
        '    <td><input id="parameter-required" type="checkbox" ' +
        (required === true ? 'checked' : '') + ' title> Required</td>' +
        '    <td>' +
        '        <div id="parameter-description" contenteditable="true" class="input-cell">' +
        (description ? description : '') + '</div>' +
        '    </td>' +
        '    <td>' +
        '        <span class="panel-controls pull-right">' +
                    '<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>' +
                    '<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>' +
                    '&nbsp;' +
        '            <a class="panel-link delete-parameter">' +
        '                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>' +
        '            </a>' +
        '        </span>' +
        '    </td>' +
        '</tr>'
    );
}


/**
 * This function remove a parameter row
 */
function removeParameterRow() {
    var row = $(this).parent().parent().parent();

    // remove the row itself
    row.remove();

    // check if there are no more rows, show the empty label in that case
    if ($('#parameter-table-body').children().length == 0) {
        // delete the table
        $('#parameter-table').remove();

        // show of the empty-table label
        $('#empty-parameters-label').show();
    }
}


/**
 * This function converts the table of parameters into a JSON string
 */
function parametersToJSON() {
    var parameters = [];
    var parameterRows = $('#parameter-table-body').children();

    for (var i = 0; i < parameterRows.length; ++i) {
        var row = parameterRows.eq(i);

        // Extract and save data
        var name = row.find('#parameter-name').text();
        var location = row.find('#parameter-location').text();
        var type = row.find('#parameter-type').text();
        var required = row.find('#parameter-required').is(':checked');
        var description = row.find('#parameter-description').text();

        // if there's no name, cannot send this
        if (!name) {
            continue;
        }

        parameters.push({
            name: name,
            location: location,
            type: type,
            required: required,
            description: description
        });
    }

    return JSON.stringify(parameters);
}


// Functions that handle the messages table
function setupMessagesTable() {
    // parameter links
    $('#add-message-link').on('click', insertMessageTable);
    $('#first-message-link').on('click', insertMessageTable);
    $(document).on('click', '.delete-message', removeMessageRow);
}


/**
 * This function inserts an empty messages table
 */
function insertMessageTable() {
    // hide of the empty-table label
    $('#empty-messages-label').hide();

    // get the messages panel body
    var messagePanel = $('#message-panel');
    if (!messagePanel.has($('#message-table')).length) {
        // Create a table
        var table = $(
            '<table id="message-table" class="table table-default">' +
            '    <thead>' +
            '    <tr>' +
            '        <th>Code</th>' +
            '        <th>Name</th>' +
            '        <th>Description</th>' +
            '        <th></th>' +
            '    </tr>' +
            '    </thead>' +
            '    <tbody id="message-table-body"></tbody>' +
            '</table>'
        ).appendTo(messagePanel);
    }

    // Now we also should insert the empty row
    addMessageRow();
}


/**
 * This function inserts a so called 'new row'to the end of the table
 */
function addMessageRow() {
    // insert the new row and set the first field in the row in focus
    generateMessageRow().appendTo($('#message-table-body')).find('#message-code').focus();
}


/**
 * This function generates the DOM node that contains the new message row. Can also be holing values passed as arguments
 */
function generateMessageRow(code, name, description) {
    return $(
        '<tr id="new-message-row">' +
        '    <td>' +
        '        <div id="message-code" contenteditable="true" class="input-cell">' +
        (code != undefined ? code : '') + '</div>' +
        '    </td>' +
        '    <td>' +
        '        <div id="message-name" contenteditable="true" class="input-cell">' +
        (name ? name : '') + '</div>' +
        '    </td>' +
        '    <td>' +
        '        <div id="message-description" contenteditable="true" class="input-cell">' +
        (description ? description : '') + '</div>' +
        '    </td>' +
        '    <td>' +
        '        <span class="panel-controls pull-right">' +
                    '<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>' +
                    '<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>' +
                    '&nbsp;' +
        '            <a class="panel-link delete-message">' +
        '                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>' +
        '            </a>' +
        '        </span>' +
        '    </td>' +
        '</tr>'
    );
}


/**
 * This function remove a message row
 */
function removeMessageRow() {
    var row = $(this).parent().parent().parent();

    // remove the row itself
    row.remove();

    // check if there are no more rows, show the empty label in that case
    if ($('#message-table-body').children().length == 0) {
        // delete the table
        $('#message-table').remove();

        // show of the empty-table label
        $('#empty-messages-label').show();
    }
}

function projectCodesToJSON() {
    var projectCodes = [];
    var projectRows = $('#project-table-body').children();

    for (var i = 0; i < projectRows.length; ++i) {
        var row = projectRows.eq(i);

        //check to see if the box is checked and if so, add it to the list
        var checkbox = row.find('.response-message-checkbox');
        var code = row.find('.response-code');
        if (checkbox.prop('checked') == true) {
            projectCodes.push(row.find('.response-code').text());
        }
    }

    return projectCodes;
}

/**
 * This function converts the table of messages into a JSON string
 */
function messagesToJSON() {
    var messages = [];
    var messageRows = $('#message-table-body').children();

    for (var i = 0; i < messageRows.length; ++i) {
        var row = messageRows.eq(i);

        // Extract and save data
        var code = row.find('#message-code').text();
        var name = row.find('#message-name').text();
        var description = row.find('#message-description').text();

        // if there's no name, cannot send this
        if (!name || !code) {
            continue;
        }

        messages.push({
            code: code,
            name: name,
            description: description
        });
    }

    return JSON.stringify(messages);
}


function upArrowClicked() {
    var row = $(this).closest('tr');
    row.insertBefore(row.prev());
}

function downArrowClicked() {
    var row = $(this).closest('tr');
    row.insertAfter(row.next());
}

function responseSelectionChange() {
    var checkbox = $(this);
    console.log('checkbox: ', checkbox);
    var responseId = checkbox.parent().parent().attr('response-id');
    var methodId = checkbox.parent().parent().attr('method-id');

    if (checkbox.prop('checked') == true) {
        console.log('checked');
        $.ajax({
            url: '/method/' + methodId + '/response/' + responseId + '/add',
            type: 'POST',
            error: function(err) {
                console.log('err: ', err);
            },
            success: function(result) {
                console.log('result: ', result);
            }
        });
    }
    else if(checkbox.prop('checked') == false) {
        console.log('not checked');
        $.ajax({
            url: '/method/' + methodId + '/response/' + responseId + '/delete',
            type: 'POST',
            error: function(err) {
                console.log('err: ', err);
            },
            success: function(result) {
                console.log('result: ', result);
            }
        });
    }
}

