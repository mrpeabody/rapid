var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var nunjucks = require('nunjucks');
var sessions = require('client-sessions');
var loadUser = require('./middleware/load_user');
var errorHandlers = require('./middleware/error_handlers');

var app = express();


// set up the template language
nunjucks.configure('views', {
    autoescape: true,
    express: app
});


//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(compression({}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'static')));


// Enable sessions
app.use(sessions({
    cookieName: 'session',
    secret: 'QufudeWrunaJ3zacH7p5phaPhadr2th7XASwE2REr2sutrUhapu75U8HEyuwredR',
    duration: 24 * 60 * 60 * 1000,
    activeDuration: 1000 * 60 * 5
}));


// Load the user (if there is one)
app.use(loadUser);
app.use(errorHandlers);


// set up URLS (routes)
app.use('/', require('./routes/session'));
app.use('/user', require('./routes/user'));
app.use('/project', require('./routes/project'));
app.use('/module', require('./routes/module'));
app.use('/page', require('./routes/page'));
app.use('/method', require('./routes/method'));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('layout/error.html', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('layout/error.html', {
        message: err.message,
        error: {}
    });
});


module.exports = exports = app;
