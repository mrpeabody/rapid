var db = require('../db/db');


// This middleware loads the user (if logged in) and assigns
// thier information to the request.user property.
// If no user is signed, in, a Guest user is created for them.
function loadUser(req, res, next) {
    if (req.session && req.session.user_id) {
        db.connect(function (err, client, done) {
            if (err) {
                return res.renderError(500, req, res);
            }
            
            client.query('SELECT * FROM api_user WHERE id = $1', [req.session.user_id], function(err, result) {
                // Return the client back to the pool
                done();

                if(err) {
                    return res.renderError(500, req, res);
                }
                else if (!result.rowCount) {
                    req.user = {username: 'Guest'};
                }
                else {
                    // sanitize the user object
                    var user = result.rows[0];
                    delete user['salt'];
                    delete user['password_digest'];
                    delete user['token'];
                    delete user['date_registered'];
                    delete user['date_modified'];

                    // Save the user in the session
                    req.user = user;
                }
                
                // Call the next function in the chain
                next();
            });
        });
    }
    else {
        req.user = {username: 'Guest'};
        next();
    }
}


module.exports = exports = loadUser;
