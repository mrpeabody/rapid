// This module wraps res.sendStatus() method, ensuring graceful handling of 404, 500, etc.


function addErrorShortcuts(req, res, next) {
    var makeError = function(code) {
        if (!code) code = 500;

        var err = new Error();
        err.status = code;
        switch (code) {
            case 403:
                err.message = 'Access restricted';
                break;
            case 404:
                err.message = 'Not Found';
                break;
            default:
                err.message = 'Internal error';
        }

        return err;
    };

    res.renderError = function(err, req, res) {
        if (!(err instanceof Error)) {
            err = makeError(err);
        }

        var dev = req.app.get('env') === 'development';

        res.status(err.status || 500);

        res.render('layout/error.html', {
            user: req.user,
            message: err.message,
            error: dev ? err : {}
        });
    };

    next();
}


module.exports = exports = addErrorShortcuts;