// Only users with admin status can continue
// down the pipeline
function adminOnly(req, res, next) {
    if (req.user && req.user.admin) {
        return next();
    }

    res.renderError(403, req, res);
}


// Only authorized users that are not in "readonly" status or are admins can continue
// down the pipeline
function canEdit(req, res, next) {
    if (req.user.username != 'Guest' && (!req.user.readonly || req.user.admin)) {
        return next();
    }

    res.renderError(403, req, res);
}


// Validates :user_id to be an id of the authorized user
function userCorrespondsWithSession(req, res, next) {
    if (req.user && req.params.user_id && req.params.user_id === req.user.id) {
        return next();
    }

    res.renderError(403, req, res);
}


// This middleware requires the current user NOT be a Guest
// Guest status is determined by the username (all guests
// share the username Guest)
function noGuests(req, res, next) {
    if (req.user.username != 'Guest') {
        return next();
    }
    
    res.render('session/new.html', {message: 'You must be signed in to access that page', user: req.user});
}


module.exports = exports = {
    adminOnly: adminOnly,
    canEdit: canEdit,
    userCorrespondsWithSession: userCorrespondsWithSession,
    noGuests: noGuests
};
