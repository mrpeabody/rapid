var mailer = require('nodemailer');
var nunjucks = require('nunjucks');

// create reusable transporter object using the default SMTP transport
var transporter = mailer.createTransport('smtps://activation%40rapid-api.me:v5puzu8uChEdEc7apahuSwEc2PaCeCha@smtp.yandex.com');

/**
 * TODO
 *
 * @param email
 * @param subject
 * @param text
 * @param html
 * @returns {undefined}
 */
function sendEmail(email, subject, text, html) {
    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"RAPID Account Activation"<activation@rapid-api.me>', // sender address
        to: email, // list of receivers
        subject: subject, // Subject line
        text: text, // plaintext body
        html: html // html body
    };
    
    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
}

/**
 * TODO
 *
 * @param email
 * @param firstName
 * @param lastName
 * @param token
 * @returns {undefined}
 */
function sendActivationEmail(email, firstName, lastName, token) {
    var textTemplate = 'Hello ' + firstName + ' ' + lastName;
    var htmlTemplate = nunjucks.render('email/activation.html', {
        firstName: firstName,
        lastName: lastName,
        token: token
    });

    sendEmail(
        email,
        'RAPID Account Activation',
        textTemplate,
        htmlTemplate
    );
}

module.exports = {
    sendActivationEmail: sendActivationEmail
};
