
/**
 * This helper function adds the current user permissions to the found project object.
 *
 * @param req
 * @param res
 * @param client
 * @param done
 * @param user
 * @param projectId
 * @param callback
 */
function projectWithPermissions(req, res, client, done, user, projectId, callback) {
    client.query(
        'SELECT project.*, permission.level FROM project ' +
        'LEFT JOIN permission ON project.id = project_id AND user_id = ($1) ' +
        'WHERE project.id = ($2);',
        [user.id, projectId], 
        function(err, result) {
            console.log('err: ', err);
            var project = result.rows[0];

            if (err || !project) {
                done();

                return res.renderError(500, req, res);
            }

            if (user.username == 'Guest' && project.is_private) {
                done();

                return res.renderError(403, req, res);
            }
            
            // return the result
            callback(project);
        }
    );
}


module.exports = {
    projectWithPermissions: projectWithPermissions
};
