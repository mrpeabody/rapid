/*
This is class that helps with paginating (i.e. splitting lists) into chunks.
 */
function Paginator(aList, elementsPerPage) {
    // Data fields (plus two from the parameters)
    var count = aList.length;
    var totalPages;

    if (count > 0 && elementsPerPage > 0) {
        if (count > elementsPerPage) {
            var ratio = count / elementsPerPage;
            totalPages = (count % elementsPerPage) == 0 ? ratio : Math.floor(ratio) + 1;
        }
        else {
            totalPages = 1;
        }
    }
    else {
        throw new Error('Invalid input(s) for Paginator were provided');
    }

    this.numberOfPages = totalPages;

    // A method to get a page (list) of chunks according to its number
    this.page = function(number) {
        // Some validation
        if (number > totalPages || number <= 0) {
            throw new Error('Page number should be between 1 and total pages count.');
        }

        return aList.slice(elementsPerPage * (number - 1), elementsPerPage * number);
    };

    // A simple method to check if the number of page is the number of the last page
    this.isLastPage = function(number) { return number == totalPages; };
}


module.exports = exports = {Paginator: Paginator};
