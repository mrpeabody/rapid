var pg = require('pg');
var config = require('../config');
var db = require('./db');
var encryption = require('./encryption');


if (!module.parent) {
    initialSetup();
}


function initialSetup(callback) {
    // Connect to the database
    db.connect(function(err, client, done) {
        if (err) {
            return console.error('Connection Error');
        }
        
        // Drop all the schema first
        client.query('DROP OWNED by ' + config.db.username, function(err) {
            // Error occured, can't continue
            if (err) {
                client.end();
                return console.error(err);
            }

            // Start creating tables
            console.log('\n\n\nThe database has been wiped');
            console.log('='.repeat(32));
            createTableUser(client, done, callback);
        });
    });
}


// Create table user
function createTableUser(client, done, callback) {
    client.query(
        'CREATE TABLE api_user (' +
        'id BIGSERIAL PRIMARY KEY, ' +
        'username TEXT UNIQUE NOT NULL, ' +
        'first_name TEXT, ' +
        'last_name TEXT, ' +
        'salt TEXT NOT NULL, ' +
        'password_digest TEXT NOT NULL, ' +
        'is_activated BOOLEAN DEFAULT FALSE, ' + 
        'token TEXT NOT NULL, ' + 
        'date_registered TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'date_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP);',
        function(err) {
            // Error occured, can't continue
            if (err) {
                client.end();
                return console.error(err);
            }

            // Call next function
            console.log('Table \033[31mapi_user\033[0m has been created');
            createTableProject(client, done, callback);
        }
    );
}


// Create table project
function createTableProject(client, done, callback) {
    client.query(
        'CREATE TABLE project (' +
        'id BIGSERIAL PRIMARY KEY, ' +
        'title VARCHAR(200) NOT NULL, ' +
        'description TEXT NOT NULL, ' +
        'url TEXT, ' +
        'response_messages TEXT, ' +
        'is_private BOOLEAN DEFAULT FALSE NOT NULL, ' +
        'date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'date_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'owner_id BIGINT REFERENCES api_user(id) ON DELETE CASCADE NOT NULL);',
        function(err) {
            // Error occured, can't continue
            if (err) {
                client.end();
                return console.error(err);
            }

            // Call next function
            console.log('Table \033[31mproject\033[0m has been created');
            createTablePermission(client, done, callback);
        }
    );
}


// Create table permissions
function createTablePermission(client, done, callback) {
    client.query(
        'CREATE TABLE permission (' +
        'id BIGSERIAL PRIMARY KEY, ' +
        'level BIGINT NOT NULL, ' +
        'date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'date_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'user_id BIGINT REFERENCES api_user(id) ON DELETE CASCADE NOT NULL, ' +
        'project_id BIGINT REFERENCES project(id) ON DELETE CASCADE NOT NULL, ' + 
        'CONSTRAINT user_project_constraint UNIQUE (user_id, project_id));',
        function(err) {
            // Error occured, can't continue
            if (err) {
                client.end();
                return console.error(err);
            }

            // Call next function
            console.log('Table \033[31mpermission\033[0m has been created');
            createTableModule(client, done, callback);
        }
    );
}


// Create table module
function createTableModule(client, done, callback) {
    client.query(
        'CREATE TABLE module (' +
        'id BIGSERIAL PRIMARY KEY, ' +
        'rank INT DEFAULT 100 NOT NULL, ' +
        'title VARCHAR(200) NOT NULL, ' +
        'description TEXT NOT NULL, ' +
        'is_private BOOLEAN DEFAULT FALSE NOT NULL, ' +
        'date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'date_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'project_id BIGINT REFERENCES project(id) ON DELETE CASCADE NOT NULL);',
        function(err) {
            // Error occured, can't continue
            if (err) {
                client.end();
                return console.error(err);
            }

            // Call next function
            console.log('Table \033[31mmodule\033[0m has been created');
            createTablePage(client, done, callback);
        }
    );
}


// Create table page
function createTablePage(client, done, callback) {
    client.query(
        'CREATE TABLE page (' +
        'id BIGSERIAL PRIMARY KEY, ' +
        'rank INT DEFAULT 100 NOT NULL, ' +
        'title VARCHAR(200) NOT NULL, ' +
        'content TEXT NOT NULL, ' +
        'is_private BOOLEAN DEFAULT FALSE NOT NULL, ' +
        'date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'date_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'project_id BIGINT REFERENCES project(id) ON DELETE CASCADE NOT NULL);',
        function(err) {
            // Error occured, can't continue
            if (err) {
                client.end();
                return console.error(err);
            }

            // Call next function
            console.log('Table \033[31mpage\033[0m has been created');
            createTableMethod(client, done, callback);
        }
    );
}


// Create table method
function createTableMethod(client, done, callback) {
    client.query(
        'CREATE TABLE method (' +
        'id BIGSERIAL PRIMARY KEY, ' +
        'rank INT DEFAULT 100 NOT NULL, ' +
        'url_name VARCHAR(100) NOT NULL, ' +
        'request_type VARCHAR(20) NOT NULL, ' +
        'short_description VARCHAR(200) NOT NULL, ' +
        'full_description TEXT, ' +
        'response TEXT, ' +
        'response_type VARCHAR(20) NOT NULL, ' +  // 0 - json, 1 - xml, 2 - plain text, etc.
        'parameters TEXT, ' +
        'project_codes TEXT, ' +
        'response_messages TEXT, ' +
        'is_private BOOLEAN DEFAULT FALSE NOT NULL, ' +
        'date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'date_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
        'project_id BIGINT REFERENCES project(id) ON DELETE CASCADE NOT NULL,' +
        'module_id BIGINT REFERENCES module(id) ON DELETE CASCADE NOT NULL);',
        function(err) {
            // Error occured, can't continue
            if (err) {
                client.end();
                return console.error(err);
            }

            // Call next function
            console.log('Table \033[31mmethod\033[0m has been created');
            createDateModifiedHandling(client, done, callback);
        }
    );
}


// Create some code to handle auto-update of timestamp
function createDateModifiedHandling(client, done, callback) {
    client.query(
        'CREATE OR REPLACE FUNCTION update_timestamp() RETURNS TRIGGER ' +
        'LANGUAGE plpgsql AS $$ ' +
        'BEGIN ' +
        'NEW.date_modified = CURRENT_TIMESTAMP; ' +
        'RETURN NEW; ' +
        'END; ' +
        '$$; ',
        function(err) {
            // Error occured, can't continue
            if (err) {
                client.end();
                return console.error(err);
            }

            console.log('Timestamp function has been created');

            client.query(
                'CREATE TRIGGER table_timestamp_update ' +
                'BEFORE UPDATE ' +
                'ON api_user ' +
                'FOR EACH ROW ' +
                'EXECUTE PROCEDURE update_timestamp();' +
                'CREATE TRIGGER table_timestamp_update ' +
                'BEFORE UPDATE ' +
                'ON permission ' +
                'FOR EACH ROW ' +
                'EXECUTE PROCEDURE update_timestamp();' +
                'CREATE TRIGGER table_timestamp_update ' +
                'BEFORE UPDATE ' +
                'ON project ' +
                'FOR EACH ROW ' +
                'EXECUTE PROCEDURE update_timestamp();' +
                'CREATE TRIGGER table_timestamp_update ' +
                'BEFORE UPDATE ' +
                'ON page ' +
                'FOR EACH ROW ' +
                'EXECUTE PROCEDURE update_timestamp();' +
                'CREATE TRIGGER table_timestamp_update ' +
                'BEFORE UPDATE ' +
                'ON method ' +
                'FOR EACH ROW ' +
                'EXECUTE PROCEDURE update_timestamp();' +
                'CREATE TRIGGER table_timestamp_update ' +
                'BEFORE UPDATE ' +
                'ON module ' +
                'FOR EACH ROW ' +
                'EXECUTE PROCEDURE update_timestamp();',
                function(err) {
                    // Error occured, can't continue
                    if (err) {
                        client.end();
                        return console.error(err);
                    }

                    // Call next function
                    console.log('Timestamp update triggers have been created');
                    createAdminUser(client, done, callback);
                }
            );
        }
    );
}


function createAdminUser(client, done, callback) {
    // Create a default user
    var salt = encryption.salt();
    var password = encryption.salt();

    client.query('INSERT INTO ' +
        'api_user (username, password_digest, salt, token, is_activated) ' +
        'values (($1), ($2), ($3), ($4), ($5))',
        ['admin', encryption.digest(password + salt), salt, encryption.salt(), true],
        function(err) {
            // Error occured, can't continue
            if (err) {
                console.error(err);
            }

            // All db operations should be completed at this point
            console.log('='.repeat(32));
            console.log('The admin user has been created with\n\tlogin: admin\n\tpassword: ' + password + '\n\n');


            if (callback !== undefined) {
                callback('admin', password);
            }

            client.end();
        }
    );
}


module.exports = initialSetup;
