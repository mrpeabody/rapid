var crypto = require('crypto');

// A randomly-generated secret string
const secret = 'Fr5dRa5aNeNape6TawRacH8cuphuSPU35acru4aprawrewu4teya3eChux553Aje';

// The algorithm to use in cyphering
const algorithm = 'aes-256-ctr';


function Encryption() {
    // Creates a random value for use as salt
    this.salt = function () {
        return crypto.randomBytes(32).toString('hex').slice(0, 32);
    };

    // Creates a cryptographic hash of the provided
    // plaintext, with additional salt using a module
    // specific secret
    this.digest = function (plaintext) {
        const hash = crypto.createHash('sha256');
        hash.update(plaintext);
        hash.update(secret);
        return hash.digest('hex');
    };

    this.encipher = function (plaintext) {
        const cipher = crypto.createCipher(algorithm, secret);
        var encrypted = cipher.update(plaintext, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        return encrypted;
    };

    this.decipher = function (crypttext) {
        const decipher = crypto.createCipher(algorithm, secret);
        var deciphered = decipher.update(crypttext, 'hex', 'utf8');
        deciphered += decipher.final('utf8');
        return deciphered;
    }
}


module.exports = new Encryption();
