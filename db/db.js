var pg = require('pg');
var config = require('../config');


var connection = process.env.DATABASE_URL || 'postgres://' +
    config.db.username + ':' +
    config.db.password +'@' +
    config.db.host + ':' +
    config.db.port + '/' +
    config.db.name;


function connect(callback) {
    pg.connect(connection, function(err, client, done) {
        if (err) {
            console.error('Error fetching client from pool:', err);
            return callback(err);
        }
        
        if (callback) {
            callback(false, client, done);
        }
    });
}


module.exports = {connect: connect};