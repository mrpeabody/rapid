$(document).ready(function() {
    $('.project-row').on('click', function() {
        // get the id of the project and go the link
        window.location.href = '/project/' + $(this).parent().attr('project-id');
    });

    // colorize the method page
    var requestType = $('#method-request-type').text();
    if (requestType) {
        colorizeMethodPage(requestType);
        highlightResponseExample();
    }

    // Add fancybox to documentation pages
    if ($.fancybox) {
        $('a.fancy').fancybox();
    }

    // smooth scroll to the top of documentation
    $('a.back-on-top').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
    });
});


/**
 * This function detects and colors request-dependent elements of the show method page
 */
function colorizeMethodPage(requestType) {
    var methodBlock = $('.method-block');
    var requestHeaderLabel = $('#method-request-type');

    // Now let's colorize stuff
    switch (requestType) {
        case 'GET':
            requestHeaderLabel.addClass('get-header');
            break;

        case 'POST':
            requestHeaderLabel.addClass('post-header');
            methodBlock.addClass('post');
            break;

        case 'PUT':
            requestHeaderLabel.addClass('put-header');
            methodBlock.addClass('put');
            break;

        case 'DELETE':
            requestHeaderLabel.addClass('delete-header');
            methodBlock.addClass('delete');
            break;
    }
}


function highlightResponseExample() {
    var textArea = document.getElementById('response-example');
    var editor = CodeMirror.fromTextArea(textArea);
}
