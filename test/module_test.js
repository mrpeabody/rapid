var module = require('../routes/module.js'),
    app = require('../app.js'),
    http = require('http'),
    assert = require('assert'),
    url = require('url'),
    globals = require('./globals'),
    port = 3000,
    server;

describe('module tests', function() {
    it('test show module', function(done) {
        http.get('http://localhost:' + port + '/module/' + globals.moduleId, function(res) {
            assert.equal(res.statusCode, 200);
            done();
        });
    });

    it('test create module', function(done) {
        http.get('http://localhost:' + port + '/project/' + globals.projectId, function(res) {
            assert.equal(res.statusCode, 200);
            done();
        })
    });

    it('test edit module', function(done) {
        http.get('http://localhost:' + port + '/module/' + globals.moduleId + '/edit', function(res) {
            assert.equal(res.statusCode, 200);
            done();
        });
    });

    it('test delete module', function(done) {
        http.get('http://localhost:' + port + '/module/' + globals.moduleId + '/delete', function(res) {
            assert.equal(res.statusCode, 200);
            done();
        });
    });

    it('test project relate new module', function(done) {
        http.get('http://localhost:' + port + '/module/project/' + globals.projectId + '/new', function(res) {
            assert.equal(res.statusCode, 200);
            done();
        });
    });
});