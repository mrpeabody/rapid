var page = require('../routes/page.js'),
    app = require('../app.js'),
    db = require('../db/db'),
    http = require('http'),
    assert = require('assert'),
    url = require('url'),
    port = 3000,
    globals = require('./globals'),
    server;

describe('page tests', function() {
    it('test project relate page id', function(done) {
        http.get('http://localhost:' + port + '/page/project/' + globals.projectId, function(res) {
            console.log(res.statusCode);
            assert.equal(res.statusCode, 404);
            done();
        });
    });

    it('test page id', function(done) {
        http.get('http://localhost:' + port + '/page/' + globals.pageId, function(res) {
            console.log(res.statusCode);
            assert.equal(res.statusCode, 200);
            done();
        });
    });

    it('test edit page', function(done) {
        http.get('http://localhost:' + port + '/page/' + globals.pageId + '/edit', function(res) {
            console.log(res.statusCode);
            assert.equal(res.statusCode, 200);
            done();
        });
    });

    it('test delete page', function(done) {
        http.get('http://localhost:' + port + '/page/' +  globals.pageId + '/delete', function(res) {
            console.log(res.statusCode);
            assert.equal(res.statusCode, 200);
            done();
        });
    });

    it('test new page', function(done) {
        http.get('http://localhost:' + port + '/page/project/' + globals.projectId + '/new', function(res) {
            console.log(res.statusCode);
            assert.equal(res.statusCode, 200);
            done();
        });
    });
});