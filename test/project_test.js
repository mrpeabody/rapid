var project = require('../routes/project.js'),
    app = require('../app.js'),
    http = require('http'),
    assert = require('assert'),
    url = require('url'),
    globals = require('./globals'),
    port = 3000,
    server;


describe('projects tests', function() {
    it('testing presentation of projects list', function(done) {
        http.get('http://localhost:' + port + '/project', function(res) {
            assert.equal(res.statusCode, 200);
            done();
        });
    });

    it('testing creating projects', function(done) {
        http.get('http://localhost:' + port + '/project/new', function(res) {
            assert.equal(res.statusCode, 200);
            done();
        });
    });


    it('test request for project page', function(done) {
        http.get('http://localhost:' + port + '/project/' + globals.projectId, function(res) {
            assert.equal(res.statusCode, 200);
            done();
        })
    });


    it('test request for project page dont exist', function(done) {
        http.get('http://localhost:' + port + '/project/0', function(res) {
            assert.equal(res.statusCode, 404);
            done();
        })
    });

    it('test request to edit project', function(done) {
        http.get('http://localhost:' + port + '/project/' + globals.projectId  + '/edit', function(res) {
            assert.equal(res.statusCode, 200);
            done();
        })
    });

    it('test request to delete project', function(done) {
        http.get('http://localhost:' + port + '/project/' + globals.projectId + '/delete', function(res) {
            assert.equal(res.statusCode, 200);
            done();
        })
    })
});