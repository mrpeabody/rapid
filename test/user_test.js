var session = require('../routes/session.js'),
    app = require('../app.js'),
    http = require('http'),
    assert = require('assert'),
    url = require('url'),
    port = 3000,
    server;

describe('user tests', function() {
    it('test user lists if user is not admin', function(done) {
        http.get('http://localhost:' + port + '/user', function(res) {
            assert.equal(res.statusCode, 403); //not admin returns 403, forbidden.
            done();
        });
    });

    it('test new user', function(done) {
        http.get('http://localhost:' + port + '/user/new', function(res) {
            assert.equal(res.statusCode, 200);
            done();
        });
    });

    it('test edit admin if user is not admin', function(done) {
        http.get('http://localhost:' + port + '/user/1/edit', function(res) {
            assert.equal(res.statusCode, 403); //not admin returns 404
            done();
        });
    });
});