var session = require('../routes/session.js'),
    app = require('../app.js'),
    http = require('http'),
    assert = require('assert'),
    url = require('url'),
    port = 3000,
    server;

describe('session tests', function() {
    it('test login', function(done) {
        http.get('http://localhost:' + port + '/login', function(res) {
            console.log(res.statusCode);
            assert.equal(res.statusCode, 200);
            done();
        });
    });

    it('test logout', function(done) {
        http.get('http://localhost:' + port + '/logout', function(res) {
            console.log(res.statusCode);
            assert.equal(res.statusCode, 200);
            done();
        });
    });
});