var app = require('../app.js'),
    project = require('../routes/project.js'),
    page = require('../routes/page.js'),
    port = 3000,
    db = require('../db/db'),
    server,
    http = require('http'),
    assert = require('assert'),
    fs = require('fs'),
    globals = require('./globals'),
    url = require('url');


before(function(testDone) {
    server = app.listen(port, function(err, result) {
        if (err) testDone(err);
    });
    
    db.connect(function(err, client, done) {
        if (err) {
            return console.error(err);
        }
        
        client.query('insert into project (title,description,url,is_private,owner_id)' +
            'values (($1), ($2), ($3), ($4), ($5)) RETURNING id',
            ['For unit testing', 'random desc', 'random url', false, 1], // Always using default user as a test user
            function(err, result) {
                if (err) {
                    done();
                    console.error(err);
                }

                globals.projectId = result.rows[0].id;

                client.query('insert into page (rank,title,content,is_private,project_id)' +
                    'values (($1), ($2), ($3), ($4), ($5)) RETURNING id;',
                    [3, 'Unit testing title', 'random content', false, globals.projectId], function(err, result) {

                        if (err) {
                            done();
                            console.error(err);
                        }

                        globals.pageId = result.rows[0].id;

                        client.query('insert into module (title,description,rank,is_private,project_id)' +
                            'values (($1), ($2), ($3), ($4), ($5)) RETURNING id;',
                            ['Unit testing title', 'random content', 3, false, globals.projectId], 
                            function(err, result) {
                                done();
                                if (err) {
                                    return console.error(err);
                                }

                                globals.moduleId = result.rows[0].id;

                                testDone();
                            }
                        );
                    }
                );
            }
        );
    });
});

after(function(testDone) {
    console.log(':()shutting down...');

    db.connect(function(err, client, done) {
        if (err) {
            return console.error(err);
        }
        
        client.query('DELETE FROM project CASCADE WHERE id=($1)', [globals.projectId],
            function(err, result) {
                console.log('clearing from project');
                if (err) console.error(err);
                done();
                testDone();
            }
        );
    });
});

describe('app tests', function() {

    it('App should exist', function() {
        assert.ok(app);
    });

    it('should be listening at ' + port, function(done) {
        http.get('http://localhost:' + port, function(res) {
            console.log(res.statusCode);
            assert.equal(res.statusCode, 200);
            done();
        });
    });


    function testStaticFile(url, path) {
        it('should serve ' + url + ' from ' + path, function(done) {
            var fileBody = fs.readFileSync(path);

            http.get('http://localhost:' + port + url, function(res) {
                assert.equal(res.statusCode, 200);
                var body = '';
                res.on('data', function(data) {
                    body += data;
                });
                res.on('end', function() {
                    assert.equal(fileBody, body);
                    done();
                });
                res.on('err', function(err) {
                    done(err);
                });
            });
        });
    }

    [
        ['/css/main.css', './static/css/main.css'],
        ['/css/method.css', './static/css/method.css'],
        ['/css/module.css', './static/css/module.css'],
        ['/css/page.css', './static/css/page.css'],
        ['/css/project.css', './static/css/project.css']

    ].forEach(function(spec) {
        testStaticFile(spec[0], spec[1]);
    });
});
